# Discord bot

## Help

Type `@Bot help` to get a list of your current scripts

## Scripting

Create new script:

`@BotTom#0236 when ^^a^^ then ^^b^^`

Add more responses to the trigger:

`@BotTom#0236 when ^^a^^ also ^^c^^`

use `^^^^` for a blank trigger. basically will fire if you just @ the bot

Breakdown is
- mention the bot
- when followed by trigger
- then followed by condition

Note: the carats ( `^^` ) are necessary

## Templates

## Triggers

No trigger templates exist currently.

### Responses

`{{random:1,5}}` 1 is the min and 5 is the max

## Development

### Getting started

You can develop purely using the unit and integration tests. Below is if you want your own bot connected to discord to use.

https://discordapp.com/developers/applications/

1. Create an application
1. View `config-template.properties` and add the appropriate information. 

### Enviroment variables

Only necessary for running on your own server.

You can check the full list in `application.properties` and `config.properties`

```
spring.datasource.url=${discord_bot_datasource_url}
spring.datasource.username=${discord_bot_datasource_username}
spring.datasource.password=${discord_bot_datasource_password}

discord.secret=${discord_secret}
discord.clientid=${discord_clientid}
discord.token=${discord_token}
```

### Databases

To get started quickly just enable the h2 options in `application.properties`

For postgres I had to mannually create the role to be used.

### ANTLR changes

Need to run the generateGrammar task for lang project

## TODO

1. Multiple triggers. Need to split the scriptName and triggers to support feature

1. I don't see a need for TRIGGER_RESPONSES so can probably delete that

Features:
  - create a script name for modification
  - delete a single response
  - delete commands sent to bot to hide the surprise
  - Requested: Reply to a specific message on a channel. Or reply when a message on a channel isn't something expected.
Bugs:

1. Bot may want to send messages in the websocket handler (like no guild_id on PMs)

2. Need to use combination of guild and channel id since people can private message and will have no guild id

- randomOnce should fire once per user not once ever

- Disallow overwriting someone else's command (Role based?)

- command to allow discord roles to create commands

- conditional flow based on user performing action (?)

- 'delete last bot message' function. If you tell the bot to add a new command should it delete your message when it adds the message to prevent ruining the surprise?

- /slap @tom#123 then "/me slaps tom around with a fish". Could mention a random user?

- slash commands

- Don't restart on connection error, do something better

- Get list of emojis from server

- Join table for context. Ex: when a specific user says something then do response

- Probabilities

- Have scripts autowire the bot which has the db connection. Use some director for multiple bots.

- guild random user, sending user functionality for bot

- scheduled tasks

- add out of context messages randomly

- add multiple users for tests

- read command to display a script's current responses

- why can't random numbers and strings all be one command???

- Need to add single response to a command. Probably also need to split the possible responses into different rows to avoid size limit

- Multiple spaces after @bot will throw off the pattern

Ideas:

1. Probability of doing a special response with a user...

Ex: 5% of the time I interact with the bot it will throw some special response back It would look like an extra response to the user



## Known issues
