package com.beatyt.discord.bot;

import com.beatyt.discord.bot.api.Bot;
import com.beatyt.discord.bot.api.CommandException;
import com.beatyt.discord.bot.api.dao.ResponseRepository;
import com.beatyt.discord.bot.api.dao.ScriptRepository;
import com.beatyt.discord.bot.impl.DiscordBotDirector;
import com.beatyt.discord.bot.impl.dao.Response;
import com.beatyt.discord.bot.impl.dao.Script;
import com.beatyt.discord.bot.impl.message.DummyMessageContext;
import com.beatyt.discord.bot.impl.message.MessageContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        Application.class
})
@TestPropertySource(locations = "classpath:application-test.properties")
public class HibernateIT {

    @Autowired
    private ResponseRepository responseRepository;

    @Autowired
    private ScriptRepository scriptRepository;

    @Autowired
    private Bot bot;

    @MockBean
    private DiscordBotDirector botDirector;

    private final MessageContext context = new DummyMessageContext();

    @Before
    public void setUp() {
        bot.getBrain().createUser(context);
    }

    @Test
    public void test() throws CommandException {
        bot.getBrain().createScript(context, "test", "x", "y");

        List<Script> scripts = scriptRepository.findByGuildId(1L);
        assertEquals(1, scripts.size());

        Iterable<Response> all = responseRepository.findAll();
        assertEquals("y", all.iterator().next().getText());
    }
}