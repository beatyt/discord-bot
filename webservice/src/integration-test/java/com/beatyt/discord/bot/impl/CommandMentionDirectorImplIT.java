/*
 * Copyright (c) 2018.
 */

package com.beatyt.discord.bot.impl;

import com.beatyt.discord.bot.api.DiscordConfig;
import com.beatyt.discord.bot.api.dao.ScriptRepository;
import com.beatyt.discord.bot.impl.dao.Response;
import com.beatyt.discord.bot.impl.dao.Script;
import com.beatyt.discord.bot.impl.dao.Trigger;
import com.beatyt.discord.bot.impl.message.DummyMessageContext;
import com.beatyt.discord.bot.impl.message.MessageContext;
import com.beatyt.discord.bot.impl.service.ResponseServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("integration-test")
@TestPropertySource("classpath:config-test.properties")
@Transactional
public class CommandMentionDirectorImplIT {

    @Autowired
    ResponseServiceImpl responseService;

    @Autowired
    ScriptRepository scriptRepository;

    @MockBean(name = "testDiscordConfig")
    DiscordConfig discordConfig;

    @MockBean
    DiscordService discordService;

    @Autowired
    CommandMentionDirectorImpl cut;

    @MockBean
    private DiscordBotDirector botDirector;

    private MessageContext dummyMessageContext = new DummyMessageContext();

    @Test
    public void itShouldCreateAScript() {
        cut.prepare("<@bot> when ^^x^^ then ^^y^^", dummyMessageContext);

        List<Script> all = scriptRepository.findByGuildIdAndScriptName(dummyMessageContext.getGuildId(), "x");
        assertEquals(1, all.size());
    }

    @Test
    public void stringsShouldNotBreakAntlrTrigger() {
        cut.prepare("<@bot> when ^^x^^ then ^^when abc^^", dummyMessageContext);

        List<Script> all = scriptRepository.findByGuildIdAndScriptName(dummyMessageContext.getGuildId(), "x");
        assertEquals(1, all.size());
        List<Response> responses = all.get(0).getResponses();
        Response response = responses.get(0);

        assertEquals("when abc", response.getText());
    }

    @Test
    public void stringsShouldNotBreakAntlrAction() {
        cut.prepare("<@bot> when ^^x^^ then ^^abc then^^", dummyMessageContext);

        List<Script> all = scriptRepository.findByGuildIdAndScriptName(dummyMessageContext.getGuildId(), "x");
        assertEquals(1, all.size());
        List<Response> responses = all.get(0).getResponses();
        Response response = responses.get(0);

        assertEquals("abc then", response.getText());
    }

    @Test
    public void testMultiline() {
        cut.prepare("<@bot> when ^^x^^ then ^^y\ny^^", dummyMessageContext);

        List<Script> all = scriptRepository.findByGuildIdAndScriptName(dummyMessageContext.getGuildId(), "x");
        assertEquals(1, all.size());
    }

    @Test
    public void testMultilineTrigger() {
        cut.prepare("<@bot> when ^^x\nx2^^ then ^^y^^", dummyMessageContext);

        List<Script> all = scriptRepository.findByGuildIdAndScriptName(dummyMessageContext.getGuildId(), "x\nx2");
        assertEquals(1, all.size());

        Script script = all.get(0);
        List<Trigger> triggers = script.getTriggers();

        assertEquals(2, triggers.size());
    }

    @Test
    public void testRandomTemplate() {
        cut.prepare("<@bot> when ^^x^^ then ^^{{random:1,1}}^^", dummyMessageContext);

        List<Script> all = scriptRepository.findByGuildIdAndScriptName(dummyMessageContext.getGuildId(), "x");
        assertEquals(1, all.size());

        Script script = all.get(0);
        Response response = script.getResponses().get(0);

        assertEquals("{{random:1,1}}", response.getText());
    }

    @Test
    public void testTemplates() {
        cut.prepare("<@bot> when ^^x^^ then ^^{{random:1,1}}^^", dummyMessageContext);

        List<Script> all = scriptRepository.findByGuildIdAndScriptName(dummyMessageContext.getGuildId(), "x");
        assertEquals(1, all.size());

        cut.prepare("<@bot> x", dummyMessageContext);
    }

    @Test
    public void testDeletes() {
        cut.prepare("<@bot> when ^^x^^ then ^^y^^", dummyMessageContext);
        List<Script> all = scriptRepository.findByGuildIdAndScriptName(dummyMessageContext.getGuildId(), "x");
        assertEquals(1, all.size());

        cut.prepare("<@bot> delete ^^x^^", dummyMessageContext);

        all = scriptRepository.findByGuildIdAndScriptName(dummyMessageContext.getGuildId(), "x");
        List<Script> collect = all.stream().filter(s -> !s.getDeleted()).collect(Collectors.toList());
        assertEquals(0, collect.size());
    }

    @Test
    public void testUpdates() {
        cut.prepare("<@bot> when ^^x^^ then ^^y^^", dummyMessageContext);
        List<Script> all = scriptRepository.findByGuildIdAndScriptName(dummyMessageContext.getGuildId(), "x");
        assertEquals(1, all.size());

        cut.prepare("<@bot> when ^^x^^ also ^^z^^", dummyMessageContext);

        all = scriptRepository.findByGuildIdAndScriptName(dummyMessageContext.getGuildId(), "x");
        List<Response> responses = all.get(0).getResponses();
        assertEquals(2, responses.size());
    }
}