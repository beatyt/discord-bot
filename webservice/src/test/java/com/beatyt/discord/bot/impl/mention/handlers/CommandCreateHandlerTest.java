/*
 * Copyright (c) 2019.
 */

package com.beatyt.discord.bot.impl.mention.handlers;

import com.beatyt.discord.bot.api.Bot;
import com.beatyt.discord.bot.impl.MockDiscordBotImpl;
import com.beatyt.discord.bot.impl.message.DummyMessageContext;
import com.beatyt.discord.bot.impl.message.MessageContext;
import org.junit.Test;

import static org.junit.Assert.*;

public class CommandCreateHandlerTest {
    private Bot bot = new MockDiscordBotImpl();

    private CommandCreateHandler cut = new CommandCreateHandler(bot);

    private final MessageContext context = new DummyMessageContext();

    @Test
    public void itShouldCreateAResponse() {

        cut.handeMention("when ^^x^^ then ^^y^^", context);

        assertEquals(1, bot.getBrain().findResponses(context, "x").size());
    }
}