package com.beatyt.discord.bot.impl.dto;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.jayway.jsonpath.JsonPath;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class MessageCreateResponse {


    @Test
    public void test() throws IOException {
        String json = Resources.toString(Resources.getResource("message-create-response.json"), Charsets.UTF_8);
        String username = JsonPath.parse(json).read("$.d.author.username");
        assertEquals(username, "Tomb the Ram of Thunder");
    }
}