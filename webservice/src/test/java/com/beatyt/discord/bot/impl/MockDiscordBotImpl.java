package com.beatyt.discord.bot.impl;

import com.beatyt.discord.bot.api.Bot;
import com.beatyt.discord.bot.impl.dao.Response;
import com.beatyt.discord.bot.impl.dao.Script;
import com.beatyt.discord.bot.impl.dao.Trigger;
import com.beatyt.discord.bot.impl.message.MessageContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MockDiscordBotImpl implements Bot, Bot.Brain {

    private List<Script> scripts = new ArrayList<>();
    private List<Response> responses = new ArrayList<>();
    private List<Trigger> triggers = new ArrayList<>();

    private long invocations = 0;

    public long getInvocations() {
        return invocations;
    }

    private String msgToSend;

    @Override
    public void send(MessageContext context, String message) {
        msgToSend = message;
    }

    @Override
    public void sendRandom(MessageContext context, String command) {
        createScript(context, command, command, command);
    }

    @Override
    public String sendResponseForUser(MessageContext context, String scriptName) {
        return "hello3";
    }

    @Override
    public Brain getBrain() {
        return this;
    }

    @Override
    public void deleteUserMessage(MessageContext context) {
    }

    @Override
    public List<Response> findResponses(MessageContext context, String rating) {
        return responses.size() > 0 ? responses : null;
    }

    @Override
    public void createScript(MessageContext context, String scriptName, String triggers, String responses) {
        Script script = new Script();
        script.setScriptName(scriptName);

        Trigger trigger = new Trigger();
        script.setTriggers(Arrays.asList(trigger));

        addResponse(context, script, responses);

        scripts.add(script);
    }

    @Override
    public void createScript(MessageContext context, String scriptName, List<String> triggers, List<String> responses) {

    }

    @Override
    public void help(MessageContext context) {

    }

    @Override
    public void saveUserData(MessageContext context, String scriptName, String text) {

    }

    @Override
    public void createUser(MessageContext context) {

    }

    @Override
    public Script findScript(MessageContext context, String triggerName) {
        return scripts.stream()
                .filter(script -> script.getScriptName().equals(triggerName))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void deleteScript(Script script) {
        scripts.remove(script);
    }

    @Override
    public void addResponse(MessageContext context, Script script, String responseText) {
        Response response = new Response();
        response.setText(responseText);
        responses.add(response);

        script.setResponses(responses);
    }

    public String getSend() {
        return msgToSend;
    }
}
