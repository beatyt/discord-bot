/*
 * Copyright (c) 2019.
 */

package com.beatyt.discord.bot.impl.message;

public class DummyMessageContext extends MessageContext {

    public DummyMessageContext() {
        this.setGuildId(1L);
        this.setUserId(1L);
        this.setUsername("test");
        this.setChannelId(1L);
    }
}