/*
 * Copyright (c) 2019.
 */

package com.beatyt.discord.bot.impl.mention.handlers;

import com.beatyt.discord.bot.api.Bot;
import com.beatyt.discord.bot.api.CommandException;
import com.beatyt.discord.bot.impl.MockDiscordBotImpl;
import com.beatyt.discord.bot.impl.dao.Script;
import com.beatyt.discord.bot.impl.message.DummyMessageContext;
import com.beatyt.discord.bot.impl.message.MessageContext;
import org.junit.Test;

import static org.junit.Assert.*;

public class CommandUpdateHandlerTest {
    private Bot bot = new MockDiscordBotImpl();

    private CommandUpdateHandler cut = new CommandUpdateHandler(bot);

    private final MessageContext context = new DummyMessageContext();

    @Test
    public void itShouldUpdate() throws CommandException {
        bot.getBrain().createScript(context, "^^x^^", "^^x^^", "^^y^^");

        cut.handeMention("when ^^x^^ then ^^y^^", context);

        assertNotNull(bot.getBrain().findScript(context, "^^x^^"));

        cut.handeMention("when ^^x^^ also ^^z^^", context);

        Script x = bot.getBrain().findScript(context, "^^x^^");
        assertEquals(2, bot.getBrain().findScript(context, "^^x^^").getResponses().size());
    }
}