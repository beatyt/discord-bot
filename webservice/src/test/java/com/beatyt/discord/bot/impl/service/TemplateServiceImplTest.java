package com.beatyt.discord.bot.impl.service;

import org.junit.Test;

import java.util.function.BiFunction;
import java.util.function.Function;

import static org.junit.Assert.*;

public class TemplateServiceImplTest {

    private TemplateServiceImpl cut = new TemplateServiceImpl();

    @Test
    public void replace() {
        String s = cut.replace("hello {{random:1,1}}");
        assertEquals("hello 1", s);

        s = cut.replace("hello {{random:abc,abc}}");
        assertEquals("hello abc", s);

        s = cut.replace("hello {{random:abc,abc}} {{random:1,1}}");
        assertEquals("hello abc 1", s);
    }

    @Test
    public void keepsRuntimeTemplate() {
        // want to keep the runtime templates and not replace them
        String s = cut.replaceOnce("hello {{random:1,1}}");
        assertEquals("hello {{random:1,1}}", s);

        s = cut.replace("hello {{random:1,1}}");
        assertEquals("hello 1", s);
    }

}