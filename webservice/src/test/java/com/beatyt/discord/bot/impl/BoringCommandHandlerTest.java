package com.beatyt.discord.bot.impl;

import com.beatyt.discord.bot.impl.message.MessageContext;
import org.junit.Test;

import static org.junit.Assert.*;

public class BoringCommandHandlerTest {

    BoringCommandHandler cut = new BoringCommandHandler();

    MessageContext context = new MessageContext();

    @Test
    public void itShouldReturnNullWhenNotMentioned() {
        String result = cut.hande("hey man", context);
        assertNull(result);
    }

    @Test
    public void spirit() {
        String result = cut.hande("<@414220327946551316> that's the spirit", context);
        assertEquals("<:ThatsTheSpirit:522412809020243990>", result);

        result = cut.hande("that's the spirit <@414220327946551316>  ", context);
        assertEquals("<:ThatsTheSpirit:522412809020243990>", result);

        result = cut.hande("storm spirit", context);
        assertEquals( "<:ThatsTheSpirit:522412809020243990>", result);

        result = cut.hande("hi storm spirit", context);
        assertEquals( "<:ThatsTheSpirit:522412809020243990>", result);
    }
}