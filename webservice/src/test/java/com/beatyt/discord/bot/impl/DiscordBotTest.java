package com.beatyt.discord.bot.impl;

import com.beatyt.discord.bot.api.dao.ResponseRepository;
import com.beatyt.discord.bot.api.dao.ScriptRepository;
import com.beatyt.discord.bot.api.dao.UserRepository;
import com.beatyt.discord.bot.impl.dao.Response;
import com.beatyt.discord.bot.impl.dao.Script;
import com.beatyt.discord.bot.impl.dao.User;
import com.beatyt.discord.bot.impl.message.MessageContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.socket.WebSocketSession;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DiscordBotTest {

    @Mock
    private ResponseRepository responseRepository;

    @Mock
    private ScriptRepository scriptRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private WebSocketSession session;

    @Mock
    private DiscordService discordService;

    @InjectMocks
    private DiscordBot cut;

    private MessageContext context = new MessageContext();
    private String scriptName = "test";

    @Before
    public void setUp() {
        context.setGuildId(1L);

        Script script = new Script();
        script.setDeleted(false);

        User user = new User();
        user.setId(1L);
        user.setName("LORDFORNGOD");


        Response responseNoUsers = new Response();
        responseNoUsers.setDeleted(false);
        responseNoUsers.setText("hello");

        Response response = new Response();
        response.setText("hello");

    }

    @Test
    public void send() {
        cut.send(context, "hello");
        verify(discordService, times(1)).call(anyString(), anyString());
    }

    @Test
    public void saveResponseForUser() {

    }

    @Test
    public void sendResponseForUser() {
        // TODO: Say you wanted a command to do something special when a specific user used it
    }

}