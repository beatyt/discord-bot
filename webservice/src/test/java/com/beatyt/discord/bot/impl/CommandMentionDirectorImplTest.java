package com.beatyt.discord.bot.impl;

import com.beatyt.discord.bot.api.Bot;
import com.beatyt.discord.bot.api.CommandMentionHandler;
import com.beatyt.discord.bot.api.DiscordConfig;
import com.beatyt.discord.bot.impl.message.DummyMessageContext;
import com.beatyt.discord.bot.impl.message.MessageContext;
import com.beatyt.discord.bot.impl.service.ResponseServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CommandMentionDirectorImplTest {

    @Mock
    List<CommandMentionHandler> handlers;

    @Mock
    ResponseServiceImpl responseService;

    @Mock
    DiscordConfig discordConfig;

    @Mock
    Bot bot;

    @InjectMocks
    CommandMentionDirectorImpl cut;

    private MessageContext context = new DummyMessageContext();
    private Bot.Brain botBrain = new MockDiscordBotImpl();

    @Before
    public void setUp() {
        when(discordConfig.getClientId()).thenReturn("bot");
        when(bot.getBrain()).thenReturn(botBrain);
    }

    @Test
    public void itShouldReturnNullWhenNotMentioned() {
        cut.prepare("hey man", context);
    }

    @Test
    public void testMultiline() {
        cut.prepare("<@bot> hey man", context);
    }
}