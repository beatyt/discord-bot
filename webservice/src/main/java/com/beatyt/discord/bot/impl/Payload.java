package com.beatyt.discord.bot.impl;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Payload {
    @JsonProperty("op")
    private String op;

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }
}
