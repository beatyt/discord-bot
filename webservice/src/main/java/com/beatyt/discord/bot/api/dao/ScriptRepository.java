package com.beatyt.discord.bot.api.dao;

import com.beatyt.discord.bot.impl.dao.Script;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ScriptRepository extends CrudRepository<Script, Long> {

    List<Script> findByGuildId(long guildId);

    List<Script> findByGuildIdAndScriptName(long guildId, String name);

    List<Script> findByChannelIdAndScriptName(long guildId, String name);
}
