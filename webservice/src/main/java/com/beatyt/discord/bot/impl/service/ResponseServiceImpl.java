package com.beatyt.discord.bot.impl.service;

import com.beatyt.discord.bot.api.dao.ResponseRepository;
import com.beatyt.discord.bot.api.dao.ScriptRepository;
import com.beatyt.discord.bot.impl.dao.Response;
import com.beatyt.discord.bot.impl.dao.Script;
import com.beatyt.discord.bot.impl.message.MessageContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ResponseServiceImpl {

    @Autowired
    private ResponseRepository dao;

    @Autowired
    private ScriptRepository scriptRepository;

    @Transactional
    public Response findRandom(MessageContext context) {
        List<Script> demo = scriptRepository.findByGuildIdAndScriptName(context.getGuildId(), "demo");
        Optional<Script> any = demo.stream().filter(d -> d.getDeleted() == false).findAny();
        List<Response> random = any.get().getResponses();
        if (random.size() > 0) {
            if (random.stream().map(Response::getRecentUseIndicator).collect(Collectors.toList()).size() == 0) {
                // If there are a lot of responses this could probably cause deadlocks
                // if you try to update all of them while someone creates new rows
                Iterable<Response> all = dao.findAll();
                for (Response response : all) {
                    response.setRecentUseIndicator(false);
                }
                dao.saveAll(all);
                return findRandom(context);
            }
            Response response = random.get(0);
            response.setRecentUseIndicator(true);
            dao.save(response);
            return response;
        } else {
            return null;
        }
    }
}
