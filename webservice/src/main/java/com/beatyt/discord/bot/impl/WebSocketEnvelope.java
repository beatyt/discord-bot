package com.beatyt.discord.bot.impl;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WebSocketEnvelope<T> {
    @JsonProperty("op")
    private final int opCode;
    @JsonProperty("d")
    private final T data;
    @JsonProperty("s")
    private final Integer sequenceNumber;
    @JsonProperty("t")
    private final String eventName;

    public WebSocketEnvelope(int opCode, T data, Integer sequenceNumber, String eventName) {
        this.opCode = opCode;
        this.data = data;
        this.sequenceNumber = sequenceNumber;
        this.eventName = eventName;
    }

    public int getOpCode() {
        return opCode;
    }

    public T getData() {
        return data;
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public String getEventName() {
        return eventName;
    }
}
