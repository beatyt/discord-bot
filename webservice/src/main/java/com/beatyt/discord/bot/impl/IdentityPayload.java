package com.beatyt.discord.bot.impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Date;
import java.util.Map;
@JsonPropertyOrder({"token","properties","compress","large_threshold","shard","presence"})
class IdentityPayload {
    @JsonProperty
    private final String token;
    @JsonProperty
    private final Map<String, String> properties;
    @JsonProperty
    private final boolean compress;
    @JsonProperty("large_threshold")
    private final long largeThreshold;
    @JsonProperty
    private final int[] shard;
    @JsonProperty
    private final Presence presence;


    IdentityPayload(String token, Map<String, String> properties, boolean compress, Long largeThreshold, int[] shard, Presence presence) {
        this.token = token;
        this.properties = properties;
        this.compress = compress;
        this.largeThreshold = largeThreshold;
        this.shard = shard;
        this.presence = presence;
    }
}
class Presence {
    @JsonProperty
    private final Game game;
    @JsonProperty
    private final String status;
    @JsonProperty
    private final Date since;
    @JsonProperty
    private final boolean afk;

    Presence(Game game, String status, Date since, boolean afk) {
        this.game = game;
        this.status = status;
        this.since = since;
        this.afk = afk;
    }
}
class Game {
    @JsonProperty
    private final String name;
    @JsonProperty
    private final long type;

    Game(String name, long type) {
        this.name = name;
        this.type = type;
    }
}
