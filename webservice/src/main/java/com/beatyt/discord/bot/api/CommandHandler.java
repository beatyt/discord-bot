package com.beatyt.discord.bot.api;

import com.beatyt.discord.bot.impl.message.MessageContext;

public interface CommandHandler {
    /**
     * Handles commands that are addressed to the entire channel. Basically anything and not bot specific
     *
     * @param command Message
     * @param context Extra info
     * @return Modified message
     */
    String hande(String command, MessageContext context);
}
