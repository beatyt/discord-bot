/*
 * Copyright (c) 2019.
 */

package com.beatyt.discord.bot.impl.mention.handlers;

import com.beatyt.discord.bot.api.Bot;
import com.beatyt.discord.bot.api.CommandMentionHandler;
import com.beatyt.discord.bot.impl.DiscordWebSockerHandler;
import com.beatyt.discord.bot.impl.dao.Script;
import com.beatyt.discord.bot.impl.dao.User;
import com.beatyt.discord.bot.impl.message.MessageContext;
import com.beatyt.discord.bot.impl.service.TemplateServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class BotCommandHandler implements CommandMentionHandler<String> {
    private Logger logger = LoggerFactory.getLogger(DiscordWebSockerHandler.class);

    private final Bot bot;
    private final TemplateServiceImpl templateService;

    @Autowired
    public BotCommandHandler(Bot bot, TemplateServiceImpl templateService) {
        this.bot = bot;
        this.templateService = templateService;
    }

    @Override
    public void handeMention(final String command, MessageContext context) {
        bot.sendRandom(context, command);

//        List<Response> responses = bot.getBrain().findResponses(context, command);
//
//        if (!responses.isEmpty()) {
//            // is there a response for this user?
//            Optional<Response> first1 = responses.stream()
//                    .filter(response -> !CollectionUtils.isEmpty(response.getUserContexts()))
//                    .filter(r -> r.getUserContexts()
//                            .stream()
//                            .anyMatch(u -> u.getId().equals(context.getUserId())))
//                    .findAny();
//            first1.ifPresent(response -> bot.send(context, response.getText()));
//            // otherwise send generic one
//            Response random = Util.random(responses);
//            String message = templateService.replace(random.getText());
//            bot.send(context, message);
//        }
    }

    private boolean userIsScriptOwner(MessageContext context, Script script) {
        return script.getUsers()
                .stream()
                .map(User::getId)
                .anyMatch(i -> i.equals(context.getUserId()));
    }
}
