package com.beatyt.discord.bot.api;

import com.beatyt.discord.bot.impl.message.MessageContext;
import org.springframework.transaction.annotation.Transactional;

public interface CommandMentionHandler<T> {
    /**
     * Command logic for a message directed at the bot
     *
     * @param command A message
     * @param context Extra information to provide
     * @return modified message
     */
    void handeMention(T command, MessageContext context);
}
