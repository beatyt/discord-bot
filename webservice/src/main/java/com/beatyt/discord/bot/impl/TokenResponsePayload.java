package com.beatyt.discord.bot.impl;

import java.util.Date;

public class TokenResponsePayload {
    private String token;
    private String scope;
    private String tokenType;
    private Date expires;

}
