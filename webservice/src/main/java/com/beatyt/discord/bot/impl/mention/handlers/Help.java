package com.beatyt.discord.bot.impl.mention.handlers;

import com.beatyt.discord.bot.api.Bot;
import com.beatyt.discord.bot.api.CommandMentionHandler;
import com.beatyt.discord.bot.impl.message.MessageContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class Help implements CommandMentionHandler<String> {

    private final Bot bot;

    @Autowired
    public Help(Bot bot) {
        this.bot = bot;
    }

    @Override
    public void handeMention(String command, MessageContext context) {
        if (command.contains("help")) {
            bot.help(context);
        }
    }
}
