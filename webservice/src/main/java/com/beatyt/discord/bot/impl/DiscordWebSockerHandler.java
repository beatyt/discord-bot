package com.beatyt.discord.bot.impl;

import com.beatyt.discord.bot.api.CommandDirector;
import com.beatyt.discord.bot.api.DiscordConfig;
import com.beatyt.discord.bot.impl.message.MessageContext;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.*;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class DiscordWebSockerHandler implements WebSocketHandler {
    private Logger logger = LoggerFactory.getLogger(DiscordWebSockerHandler.class);

    private final DiscordConfig discordConfig;
    private final List<CommandDirector> delegates;
    private final DiscordConnectionObserver connectionObserver;

    public DiscordWebSockerHandler(DiscordConfig discordConfig, DiscordConnectionObserver connectionObserver, CommandDirector... delegates) {
        this.discordConfig = discordConfig;
        this.connectionObserver = connectionObserver;
        this.delegates = Arrays.asList(delegates);
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        connectionObserver.update(session);
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        if (message instanceof BinaryMessage) {
            // Why do we get these binary messages? How to handle?
            return;
        }
        final String msg = String.valueOf(message.getPayload());
        logger.info(msg);
        JSONObject jsonObject = new JSONObject(msg);
        final int op = jsonObject.getInt("op");

        String response = "";
        if (op == OpCodes.HELLO.getOpCode()) {
            // We are now connected
            response = createIdentificationPayload();
            scheduleHeartbeat(session, jsonObject);
        } else if (op == OpCodes.DISPATCH.getOpCode()) {
            String t = jsonObject.getString("t");
            if (t.equals("PRESENCES_REPLACE")) {
                // d is array
            } else if (t.equals("PRESENCES_UPDATE")) {

            } else if (t.equals("CHANNEL_CREATE")) {
                JSONObject d = jsonObject.getJSONObject("d");
                d.getString("id");
            } else if (t.equals("MESSAGE_CREATE")) {
                JSONObject d = jsonObject.getJSONObject("d");
                if (senderIsBot(d)) return;

                Configuration configuration = Configuration.builder().options(Option.DEFAULT_PATH_LEAF_TO_NULL).build();
                DocumentContext parse = JsonPath.using(configuration).parse(msg);

                String username = parse.read("$.d.author.username");
                String userId = parse.read("$.d.author.id");
                String guildId = parse.read("$.d.guild_id");
                String channelId = parse.read("$.d.channel_id");
                String messageId = parse.read("$.d.id");

                // delegate
                String content = d.getString("content");
                MessageContext context = new MessageContext()
                        .setGuildId(guildId != null ? Long.valueOf(guildId) : null)
                        .setUserId(Long.valueOf(userId))
                        .setUsername(username)
                        .setChannelId(Long.valueOf(channelId))
                        .setMessageId(Long.valueOf(messageId));

                delegates.forEach(p -> p.prepare(content, context));
                return;
//                WebSocketEnvelope<String> message_create = new WebSocketEnvelope<>(OpCodes.STATUS_UPDATE.getOpCode(), handled, null, "MESSAGE_CREATE");
//                ObjectMapper mapper = new ObjectMapper();
//                response = mapper.writeValueAsString(message_create);
            }
        } else if (op == OpCodes.HEARTBEAT_ACK.getOpCode()) {
        } else {
            logger.warn("Unknown op {}", op);
            return;
        }
        if (StringUtils.isEmpty(response)) {
            return;
        }
        logger.info(response);
        TextMessage textMessage = new TextMessage(response);
        session.sendMessage(textMessage);
    }

    private boolean senderIsBot(JSONObject d) {
        return d.getJSONObject("author").has("bot");
    }

    private void scheduleHeartbeat(WebSocketSession session, JSONObject jsonObject) {
        JSONObject data = jsonObject.getJSONObject("d");
        final int heatbeatInterval = data.getInt("heartbeat_interval");
        // Refactor below out
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        JSONObject heartbeatObject = new JSONObject();
        heartbeatObject.put("op", 1);
        heartbeatObject.put("d", jsonObject.get("s"));
        String s = heartbeatObject.toString();
        executorService.scheduleAtFixedRate(() -> {
            try {
                session.sendMessage(new TextMessage(s));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }, 0, heatbeatInterval, TimeUnit.MILLISECONDS);
    }

    private String createIdentificationPayload() throws JsonProcessingException {
        // Need to include the op code, below is the data
        // https://discordapp.com/developers/docs/topics/gateway#payloads
        Map<String, String> properties = new HashMap<>();
        properties.put("$os", "linux");
        properties.put("$browser", "disco");
        properties.put("$device", "disco");
        // TODO: Config the game, status for presence
        Presence presence = new Presence(new Game("Dota 2", 0), "dnd", new Date(), false);

        IdentityPayload identityPayload = new IdentityPayload(discordConfig.getToken(), properties, true, 250L, new int[]{0, 1}, presence);

        WebSocketEnvelope<IdentityPayload> identityPayloadWebSocketEnvelope = new WebSocketEnvelope<>(2, identityPayload, null, null);

        ObjectMapper mapper = new ObjectMapper();
        String s = mapper.writeValueAsString(identityPayloadWebSocketEnvelope);

        return s;
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) {
        logger.error("F");
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) {
        logger.info("Connection closed." + String.valueOf(closeStatus.getCode()));
        connectionObserver.closed(session);
    }

    @Override
    public boolean supportsPartialMessages() {
        return true;
    }
}
