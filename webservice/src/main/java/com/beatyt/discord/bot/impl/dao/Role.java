package com.beatyt.discord.bot.impl.dao;

import javax.persistence.*;
import java.util.List;

@Entity(name = "ROLES")
public class Role extends TimestampFields {

    @Id
    private Long id;

    @Column
    private String roleName;

    @OneToMany
    @JoinColumn
    private List<Permission> permissions;
}
