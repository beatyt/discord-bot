package com.beatyt.discord.bot.api.dao;

import com.beatyt.discord.bot.impl.dao.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

}
