package com.beatyt.discord.bot.api;

import com.beatyt.discord.bot.impl.dao.Response;
import com.beatyt.discord.bot.impl.dao.Script;
import com.beatyt.discord.bot.impl.message.MessageContext;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface Bot extends HelpMixin {

    void send(@NonNull MessageContext context, @NonNull String message);

    void sendRandom(MessageContext context, String message);

    String sendResponseForUser(MessageContext context, String scriptName);

    // hubot did it first
    Bot.Brain getBrain();

    void deleteUserMessage(MessageContext context);

    @Transactional
    interface Brain { // TODO: Mixins
        List<Response> findResponses(MessageContext context, String command);

        void createScript(MessageContext context, String scriptName, @Nullable String triggers, String responses) throws CommandException;

        void createScript(MessageContext context, String scriptName, @Nullable List<String> triggers, List<String> responses);

        void saveUserData(MessageContext context, String scriptName, String text);

        void createUser(MessageContext context);

        Script findScript(MessageContext context, String scriptName);

        void deleteScript(Script script);

        void addResponse(MessageContext context, Script script, String responseText);
    }
}
