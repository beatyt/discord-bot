package com.beatyt.discord.bot.impl;

import com.beatyt.discord.bot.api.Bot;
import com.beatyt.discord.bot.api.CommandException;
import com.beatyt.discord.bot.api.DiscordConfig;
import com.beatyt.discord.bot.api.dao.ResponseRepository;
import com.beatyt.discord.bot.api.dao.ScriptRepository;
import com.beatyt.discord.bot.api.dao.TriggerRepository;
import com.beatyt.discord.bot.api.dao.UserRepository;
import com.beatyt.discord.bot.impl.dao.Response;
import com.beatyt.discord.bot.impl.dao.Script;
import com.beatyt.discord.bot.impl.dao.Trigger;
import com.beatyt.discord.bot.impl.dao.User;
import com.beatyt.discord.bot.impl.message.MessageContext;
import com.beatyt.discord.bot.impl.service.TemplateServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Transactional
public class DiscordBot implements Bot, Bot.Brain {
    private Logger logger = LoggerFactory.getLogger(DiscordBot.class);

    private WebSocketSession session;

    public void setSession(WebSocketSession session) {
        this.session = session;
    }

    @Autowired
    private TriggerRepository triggerRepository;

    @Autowired
    private ResponseRepository responseRepository;

    @Autowired
    private ScriptRepository scriptRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TemplateServiceImpl templateService;

    @Autowired
    private DiscordService discordService;

    @Autowired
    @Qualifier("discordConfig")
    private DiscordConfig config;

    @Override
    public void send(@NonNull MessageContext context, @NonNull String message) {
        try {
            // getting url
            Long channel_id = context.getChannelId();
            String url = Resources.BASE_URL + Resources.Resource.CREATE_MESSAGE.getResource();
            url = String.format(url, channel_id);
            logger.info(url);

            // Creating body
            logger.info(message);
            CreateMessage createMessage = new CreateMessage(message, false);
            ObjectMapper mapper = new ObjectMapper();
            String requestBody = mapper.writeValueAsString(createMessage);

            // Create message call
            discordService.call(url, requestBody);

        } catch (IOException e) {
            logger.error("Could not send message to discord", e);
        }
    }

    @Override
    public void sendRandom(@NonNull MessageContext context, @NonNull String command) {
        List<Response> responses = findResponses(context, command);
        if (responses == null) {
            return;
        }
        List<Response> oldResponses = responses.stream().filter(r -> r.getRecentUseIndicator() == false).collect(Collectors.toList());
        if (oldResponses.size() == 0) {
            responses.forEach(r -> r.setRecentUseIndicator(false));
            responseRepository.saveAll(responses);
            sendRandom(context, command);
        } else {
            Response random = Util.random(oldResponses);
            random.setRecentUseIndicator(true);
            send(context, random.getText());
        }
    }

    @Override
    public String sendResponseForUser(@NonNull MessageContext context, @NonNull String scriptName) {
        Script script = findScript(context, scriptName);
        Optional<User> byId = userRepository.findById(context.getUserId());
        if (byId.isPresent()) {
            List<Response> rating = script.getResponses().stream().filter(response -> response.getUserContexts().size() > 0).collect(Collectors.toList());
            return rating.get(0).getText();
        }
        return null;
    }

    @Override
    public void deleteUserMessage(MessageContext context) {
        String url = Resources.BASE_URL + Resources.Resource.DELETE_MESSAGE.getResource();
        url = String.format(url, context.getChannelId(), context.getMessageId() );

        CreateMessage createMessage = new CreateMessage(null, false);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String string = objectMapper.writeValueAsString(createMessage);
            discordService.delete(url, string);
        } catch (JsonProcessingException e) {
            logger.error("Could not serialize", e);
        }
    }

    @Override
    public Brain getBrain() {
        return this;
    }

    @Override
    public List<Response> findResponses(@NonNull MessageContext context, @NonNull String command) {
        Script script = findScript(context, command);
        if (script == null) return null;
        return script.getResponses();
//        List<Trigger> triggers = script.getTriggers();
//        List<List<Response>> collect = triggers.stream()
//                .filter(trigger -> trigger.getText().equals(command))
//                .map(t -> {
//                    List<Response> responses = t.getResponses();
//                    return responses.stream()
//                            .filter(r -> CollectionUtils.isEmpty(r.getUserContexts()))
//                            .collect(Collectors.toList());
//                }).collect(Collectors.toList());
//
//        return collect.stream().findAny().get();
    }

    @Override
    public void createScript(@NonNull MessageContext context, @NonNull String scriptName, @NonNull String triggerText, @NonNull String responseText) throws CommandException {
        scriptName = scriptName.replace("^^", "");
        triggerText = triggerText.replace("^^", "");
        responseText = responseText.replace("^^", "");
        // Should probably find script based on matching triggers, no?
        Script existingScript = findScript(context, scriptName);
        if (existingScript != null) {
            throw new CommandException("There is already a command for " + scriptName);
        }
        Script script = new Script();
        script.setDeleted(false);
        script.setChannelId(context.getChannelId());
        script.setGuildId(context.getGuildId());
        script.setScriptName(scriptName);
        script.setCreatedAt(new Date());

        Optional<User> optionalUser = userRepository.findById(context.getUserId());
        List<User> users = new ArrayList<>(Arrays.asList(optionalUser.get()));
        script.setUsers(users);

        String[] split = responseText.split("\n");
        List<String> responseTexts = new ArrayList<>(Arrays.asList(split));
        List<Response> createdResponses = responseTexts.stream()
                .map(this::createResponse)
                .collect(Collectors.toList());

        String[] triggerSplit = triggerText.split("\n");
        List<String> triggerTexts = new ArrayList<>(Arrays.asList(triggerSplit));
        List<Trigger> createdTriggers = triggerTexts.stream()
                .map(t-> createTrigger(t, createdResponses))
                .collect(Collectors.toList());

        // TODO: If a special action should happen for a specific user then set a user context
//        trigger.setUserContexts(users);

        triggerRepository.saveAll(createdTriggers);

        script.setTriggers(createdTriggers);

        script.setResponses(createdResponses);

        scriptRepository.save(script);
    }

    private Trigger createTrigger(String t, List<Response> responses) {
        Trigger trigger = new Trigger();
        trigger.setText(t);
        trigger.setDeleted(false);
        trigger.setCreatedAt(new Date());
        trigger.setResponses(responses);

        return trigger;
    }

    private Response createResponse(String t) {
        Response response = new Response();
        // TODO: If a special action should happen for a specific user then set a user context
//        response.setUserContexts(users);
        response.setText(t);
        response.setProbability(new BigDecimal(100));
        response.setRecentUseIndicator(false);
        response.setDeleted(false);
        response.setCreatedAt(new Date());

        List<Response> responses = new ArrayList<>();
        responses.add(response);

        responseRepository.save(response);

        return response;
    }

    @Override
    public void createScript(@NonNull MessageContext context, @NonNull String scriptName, @Nullable List<String> triggers, @NonNull List<String> responses) {
        triggers.forEach(t -> responses.forEach(r -> {
            try {
                createScript(context, scriptName, t, r);
            } catch (CommandException e) {
                logger.error("Could not create command" + scriptName, e);
            }
        }));
    }

    @Override
    public void help(@NonNull MessageContext context) {
        String s = scriptRepository.findByGuildId(context.getGuildId())
                .stream()
                .filter(script -> !script.getDeleted())
                .map(Script::getScriptName)
                .collect(Collectors.joining(", "));
        send(context, s);
    }

    @Override
    public void saveUserData(@NonNull MessageContext context, @NonNull String text, @NonNull String scriptName) {
        Script script = findScript(context, scriptName);

        User user = getUser(context);

        List<Response> responses = script.getResponses();
        responses.forEach(response -> response.setUserContexts(Collections.singletonList(user)));

    }

    @Override
    public void createUser(@NonNull MessageContext context) {
        User user = getUser(context);
        Optional<User> existingUser = userRepository.findById(user.getId());
        if (!existingUser.isPresent()) {
            userRepository.save(user);
        }
    }

    @Override
    public Script findScript(@NonNull MessageContext context, @NonNull String triggerName) {
        triggerName = triggerName.replace("^^", "");
        return scriptRepository
                .findByGuildIdAndScriptName(context.getGuildId(), triggerName)
                .stream()
                .filter(s -> s.getDeleted() == false)
                .findAny()
                .orElse(null);
    }

    @Override
    public void deleteScript(@NonNull Script script) {
        script.setDeleted(true);
        script.getTriggers().forEach(t -> t.setDeleted(true));
        script.getResponses().forEach(r -> r.setDeleted(true));
        scriptRepository.save(script);
    }

    @Override
    public void addResponse(MessageContext context, Script script, String responseText) {
        responseText = responseText.replace("^^", "");
        Response response = createResponse(responseText);

        List<Response> responses1 = script.getResponses();
        responses1.add(response);

        scriptRepository.save(script);
    }

    private User getUser(@NonNull MessageContext context) {
        User user = new User();
        user.setId(context.getUserId());
        user.setName(context.getUsername());
        return user;
    }
}
