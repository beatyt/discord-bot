/*
 * Copyright (c) 2019.
 */

package com.beatyt.discord.bot.impl.mention.handlers;

import com.beatyt.discord.bot.api.Bot;
import com.beatyt.discord.bot.api.CommandMentionHandler;
import com.beatyt.discord.bot.impl.DiscordWebSockerHandler;
import com.beatyt.discord.bot.impl.dao.Script;
import com.beatyt.discord.bot.impl.message.MessageContext;
import com.beatyt.lang.CommandAdditionImpl;
import com.beatyt.lang.CommandDeletionImpl;
import com.beatyt.lang.api.commands.Deletion;
import com.beatyt.lang.api.commands.Update;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommandDeleteHandler implements CommandMentionHandler<String> {
    private Logger logger = LoggerFactory.getLogger(DiscordWebSockerHandler.class);
    private final Bot bot;

    @Autowired
    public CommandDeleteHandler(Bot bot) {
        this.bot = bot;
    }

    @Override
    public void handeMention(String command, MessageContext context) {
        // deletes
        Deletion result = new CommandDeletionImpl().execute(command);
        String triggerName = result.getTrigger();
        if (StringUtils.isEmpty(result.getErrors())) {
            // Needs the script name or trigger to delete
            Script script = bot.getBrain().findScript(context, triggerName);
            /*else if (!userIsScriptOwner(context, script)) {
                return "Check your privilege. This isn't your script.";
            } */if (script != null) {
                bot.getBrain().deleteScript(script);
                bot.send(context, "Deleted " + script.getScriptName());
                bot.deleteUserMessage(context);
            }
        }
    }
}
