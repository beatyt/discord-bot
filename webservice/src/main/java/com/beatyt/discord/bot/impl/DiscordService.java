/*
 * Copyright (c) 2019.
 */

package com.beatyt.discord.bot.impl;

import com.beatyt.discord.bot.api.DiscordConfig;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class DiscordService {
    private Logger logger = LoggerFactory.getLogger(DiscordService.class);

    @Autowired
    @Qualifier("discordConfig")
    private DiscordConfig config;

    public void delete(String url, String requestBody) {
        logger.info("Request to discord: {}", requestBody);
        try {
            // Create message call
            HttpResponse<JsonNode> response = Unirest.delete(url)
                    .header("Authorization", "Bot " + config.getToken())
                    .header("Content-Type", "application/json")
                    .asJson();
            logger.info("Response from discord: {}", response.getStatus());

        } catch (UnirestException e) {
            logger.error("Could not send message to discord", e);
        }
    }

    public void call(String url, String requestBody) {
        logger.info("Request to discord: {}", requestBody);
        try {
            // Create message call
            String s = Unirest.post(url)
                    .header("Authorization", "Bot " + config.getToken())
                    .header("Content-Type", "application/json")
                    .body(requestBody)
                    .asJson()
                    .getBody()
                    .toString();
            logger.info("Response from discord: {}", s);

        } catch (UnirestException e) {
            logger.error("Could not send message to discord", e);
        }
    }
}
