package com.beatyt.discord.bot.impl;

import com.beatyt.discord.bot.api.Bot;
import com.beatyt.discord.bot.api.CommandDirector;
import com.beatyt.discord.bot.api.CommandMentionHandler;
import com.beatyt.discord.bot.api.DiscordConfig;
import com.beatyt.discord.bot.impl.message.MessageContext;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class CommandMentionDirectorImpl implements CommandDirector {

    private final DiscordConfig discordConfig;
    @Autowired
    private final List<CommandMentionHandler<String>> handlers;
    @Autowired
    private final Bot bot;

    public CommandMentionDirectorImpl(DiscordConfig discordConfig, List<CommandMentionHandler<String>> handlers, Bot bot) {
        this.discordConfig = discordConfig;
        this.handlers = handlers;
        this.bot = bot;
    }

    @Override
    public void prepare(String command, MessageContext context) {
        bot.getBrain().createUser(context);
        final String[] s = command.split(" ");
        boolean mentioned = false;
        for (String str : s) {
            if (str.equals("<@" + discordConfig.getClientId() + ">")) {
                mentioned = true;
            }
        }
        if (!mentioned) {
            return;
        }
        final String[] remaining = Arrays.copyOfRange(s, 1, s.length);
        final String cmd = StringUtils.join(remaining, " ");
        handlers.forEach(h -> h.handeMention(cmd, context));
    }
}
