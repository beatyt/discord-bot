package com.beatyt.discord.bot.impl;

public class Resources {
    static String BASE_URL = "https://discordapp.com/api/v6";

    enum Resource {
        // Channels
        GET_CHANNELS("GET", "/channels/%s"),
        CREATE_MESSAGE("POST","/channels/%s/messages"),
        DELETE_MESSAGE("DELETE","/channels/%s/messages/%s"), // channel.id, message.id
        // Guilds
        GET_GUILD("GET","/guilds/%s"),
        GET_GUILD_MEMBERS("GET","/guilds/%s/members");

        Resource(String method, String resource) {
            this.method = method;
            this.resource = resource;
        }
        private final String method;
        private final String resource;

        public String getMethod() {
            return method;
        }

        public String getResource() {
            return resource;
        }
    }

}
