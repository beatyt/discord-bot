/*
 * Copyright (c) 2019.
 */

package com.beatyt.discord.bot.impl;


import com.beatyt.discord.bot.api.Adapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.WebSocketSession;

public class DiscordConnectionObserver {
    private Logger logger = LoggerFactory.getLogger(DiscordConnectionObserver.class);

    private final Adapter instantiator;

    public DiscordConnectionObserver(Adapter instantiator) {
        this.instantiator = instantiator;
    }

    public void update(WebSocketSession session) {
        instantiator.getBots().forEach(bot -> ((DiscordBot) bot).setSession(session));
    }

    public void closed(WebSocketSession session) {
        instantiator.connect();
    }
}
