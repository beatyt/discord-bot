/*
 * Copyright (c) 2019.
 */

package com.beatyt.discord.bot.impl;

import com.beatyt.discord.bot.api.Adapter;
import com.beatyt.discord.bot.api.Bot;
import com.beatyt.discord.bot.api.CommandDirector;
import com.beatyt.discord.bot.api.DiscordConfig;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.client.WebSocketConnectionManager;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;

import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;

@Component
public class DiscordBotDirector implements Adapter {
    private Logger logger = LoggerFactory.getLogger(DiscordBotDirector.class);

    private final List<Bot> bots;
    private final CommandDirector delegate2;
    private final DiscordConfig discordConfig;
    private final CommandDirector delegate;
    private ScheduledExecutorService executorService = new ScheduledThreadPoolExecutor(5);
    private WebSocketConnectionManager connectionManager;
    @Autowired
    public DiscordBotDirector(List<Bot> bots, @Qualifier("commandDirectorImpl") CommandDirector delegate2, DiscordConfig discordConfig, @Qualifier("commandMentionDirectorImpl") CommandDirector delegate) {
        this.bots = bots;
        this.delegate2 = delegate2;
        this.discordConfig = discordConfig;
        this.delegate = delegate;
        logger.info("Please share this url with users to allow your adapter to join: " + this.discordConfig.getUrl());
    }

    @Override
    public void connect() {
        DiscordConnectionObserver connectionObserver = new DiscordConnectionObserver(this);
        connectionManager = new WebSocketConnectionManager(new StandardWebSocketClient(), new DiscordWebSockerHandler(discordConfig, connectionObserver, delegate, delegate2), discordConfig.getApiUrl());
        connectionManager.start();
        executorService.submit(this);
    }

    @Override
    public void reconnect() {
        connectionManager.start();
    }

    @Override
    public List<Bot> getBots() {
        return bots;
    }

    // FIXME: Use this to update config for use in the WebSocket communication and remove from config.properties
    private String getToken() {
        try {
            JSONObject object = Unirest.post("https://discordapp.com/api/oauth2/token")
                    .basicAuth(discordConfig.getClientId(), discordConfig.getSecret())
                    .queryString("grant_type", "client_credentials")
                    .queryString("scope", "adapter")
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .asJson()
                    .getBody()
                    .getObject();
            logger.info(object.toString());
            return (String) object.get("access_token");
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void run() { }
}
