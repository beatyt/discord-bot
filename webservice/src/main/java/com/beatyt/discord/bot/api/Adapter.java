package com.beatyt.discord.bot.api;

import java.util.List;

public interface Adapter extends Runnable {
    /**
     * Performs the connection to a service
     */
    void connect();

    void reconnect();

    List<Bot> getBots();
}
