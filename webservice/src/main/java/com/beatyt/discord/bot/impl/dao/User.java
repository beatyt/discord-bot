package com.beatyt.discord.bot.impl.dao;

import javax.persistence.*;
import java.util.List;

@Entity(name = "USERS")
public class User extends TimestampFields {

    @Id
    private Long id;

    @Column
    private String name;

    @OneToMany
    @JoinColumn
    private List<Role> roles;

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
