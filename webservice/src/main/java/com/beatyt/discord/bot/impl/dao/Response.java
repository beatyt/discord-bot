package com.beatyt.discord.bot.impl.dao;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity(name = "RESPONSES")
public class Response extends TimestampFields {

    @Id // Maybe need composite key of channel/guild/script name for performance
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 4000)
    private String text;

    @Column(nullable = false)
    private boolean recentUseIndicator;

    @Column(nullable = false)
    private BigDecimal probability;

    @ManyToMany
    private List<User> userContexts;

    @Column(nullable = false)
    private boolean deleted;

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public List<User> getUserContexts() {
        return userContexts;
    }

    public void setUserContexts(List<User> userContexts) {
        this.userContexts = userContexts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getRecentUseIndicator() {
        return recentUseIndicator;
    }

    public void setRecentUseIndicator(Boolean recentUseIndicator) {
        this.recentUseIndicator = recentUseIndicator;
    }

    public BigDecimal getProbability() {
        return probability;
    }

    public void setProbability(BigDecimal probability) {
        this.probability = probability;
    }

}
