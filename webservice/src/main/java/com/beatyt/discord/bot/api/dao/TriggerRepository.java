/*
 * Copyright (c) 2019.
 */

package com.beatyt.discord.bot.api.dao;

import com.beatyt.discord.bot.impl.dao.Trigger;
import org.springframework.data.repository.CrudRepository;

public interface TriggerRepository extends CrudRepository<Trigger, Long> {

}
