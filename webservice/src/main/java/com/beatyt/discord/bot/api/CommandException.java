package com.beatyt.discord.bot.api;

public class CommandException extends Exception {
    public CommandException(String s) {
        super(s);
    }
}
