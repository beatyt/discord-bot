package com.beatyt.discord.bot.impl;

import com.beatyt.discord.bot.api.CommandHandler;
import com.beatyt.discord.bot.impl.message.MessageContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class CommandDirectorImpl implements com.beatyt.discord.bot.api.CommandDirector {

    private final List<CommandHandler> handlers;

    @Autowired
    public CommandDirectorImpl(List<CommandHandler> handlers) {
        this.handlers = handlers;
    }

    @Override
    public void prepare(String command, MessageContext context) {
        handlers.forEach(h -> h.hande(command, context));
    }
}
