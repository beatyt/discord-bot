package com.beatyt.discord.bot.impl;

import com.beatyt.discord.bot.api.CommandHandler;
import com.beatyt.discord.bot.impl.message.MessageContext;
import org.springframework.stereotype.Component;

@Component
public class BoringCommandHandler implements CommandHandler {

    @Override
    public String hande(String command, MessageContext context) {
        command = command.toLowerCase();
        if (command.contains("storm")
                || command.contains("thats the spirit")
                || command.contains("that's the spirit")) {
            return "<:ThatsTheSpirit:522412809020243990>";
        }
        return null;
    }

}
