/*
 * Copyright (c) 2019.
 */

package com.beatyt.discord.bot.impl.mention.handlers;

import com.beatyt.discord.bot.api.Bot;
import com.beatyt.discord.bot.api.CommandException;
import com.beatyt.discord.bot.api.CommandMentionHandler;
import com.beatyt.discord.bot.impl.DiscordWebSockerHandler;
import com.beatyt.discord.bot.impl.dao.Script;
import com.beatyt.discord.bot.impl.message.MessageContext;
import com.beatyt.lang.CommandCreatorImpl;
import com.beatyt.lang.CommandDeletionImpl;
import com.beatyt.lang.api.commands.Create;
import com.beatyt.lang.api.commands.Deletion;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommandCreateHandler implements CommandMentionHandler<String> {
    private Logger logger = LoggerFactory.getLogger(DiscordWebSockerHandler.class);
    private final Bot bot;

    @Autowired
    public CommandCreateHandler(Bot bot) {
        this.bot = bot;
    }

    @Override
    public void handeMention(String command, MessageContext context) {
        Create create = new CommandCreatorImpl().execute(command);
        String trigger = create.getTrigger();
        String action = create.getAction();
        String errors = create.getErrors();

        if (StringUtils.isEmpty(errors)) {
            try {
                bot.getBrain().createScript(context, trigger, trigger, action);
                bot.send(context,"Added command " + trigger);
                bot.deleteUserMessage(context);
            } catch (CommandException e) {
                bot.send(context, "There is already a command: " + trigger);
            }
        }
    }
}
