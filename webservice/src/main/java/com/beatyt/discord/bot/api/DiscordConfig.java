package com.beatyt.discord.bot.api;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.test.context.TestPropertySource;


@PropertySources({
        @PropertySource("classpath:config.properties"),
        @PropertySource(value="classpath:config-${discord.bot.env}.properties", ignoreResourceNotFound = true)
})
@EnableConfigurationProperties(DiscordConfig.class)
@ConfigurationProperties
@Configuration("discordConfig")
public class DiscordConfig {

    @Value("${bot.name}")
    private String botName;
    @Value("${discord.url}")
    private String url;
    @Value("${discord.secret}")
    private String secret;
    @Value("${discord.clientid}")
    private String clientId;
    @Value("${discord.permissions}")
    private String permissions;
    @Value("${discord.api.url}")
    private String apiUrl;
    @Value("${discord.token}")
    private String token;

    public String getBotName() {
        return botName;
    }

    public String getUrl() {
        return url;
    }

    public String getSecret() {
        return secret;
    }

    public String getClientId() {
        return clientId;
    }

    public String getPermissions() {
        return permissions;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public String getToken() {
        return token;
    }
}
