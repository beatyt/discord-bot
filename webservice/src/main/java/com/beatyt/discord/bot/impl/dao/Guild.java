package com.beatyt.discord.bot.impl.dao;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "GUILDS")
@Entity
public class Guild extends TimestampFields {

    @Id
    private Long guildId;
    private String name;
    private String region;
    private Integer verificationLevel;
    private Integer defaultMessageNotifications;
    private Integer explicitContentFilter;
    private Integer afkChannelId;
    private Integer afkTimeout;
    private String icon;
    private Integer ownerId;
    private String splash;
    private Integer systemChannelId;

    public Long getGuildId() {
        return guildId;
    }

    public void setGuildId(Long id) {
        this.guildId = id;
    }
}
