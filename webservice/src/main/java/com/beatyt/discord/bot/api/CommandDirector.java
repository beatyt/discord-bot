package com.beatyt.discord.bot.api;

import com.beatyt.discord.bot.impl.message.MessageContext;

public interface CommandDirector {
    /**
     * Performs pre-processing and then hands off command to the handlers
     *
     * @param command string to evaluate
     * @param context extra information
     * @return modified string
     */
    void prepare(String command, MessageContext context);
}
