package com.beatyt.discord.bot.impl.dao;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

@Entity(name = "PERMISSIONS")
public class Permission extends TimestampFields {

    @Id
    private Long id;

    @Column(nullable = false)
    private Long scriptId;

    @Column
    private String name;

    @Column
    @ColumnDefault(value = "0")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean read;

    @Column
    @ColumnDefault(value = "0")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean write;

    @Column
    @ColumnDefault(value = "0")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean execute;

}
