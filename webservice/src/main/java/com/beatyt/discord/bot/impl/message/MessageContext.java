package com.beatyt.discord.bot.impl.message;

public class MessageContext {
    private String username;
    private Long userId;
    private Long channelId;
    private Long guildId;
    private Long messageId;

    public String getUsername() {
        return username;
    }

    public MessageContext setUsername(String username) {
        this.username = username;
        return this;
    }

    public Long getUserId() {
        return userId;
    }

    public MessageContext setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public Long getGuildId() {
        return guildId;
    }

    public MessageContext setGuildId(Long guildId) {
        this.guildId = guildId;
        return this;
    }

    public Long getChannelId() {
        return channelId;
    }

    public MessageContext setChannelId(Long channelId) {
        this.channelId = channelId;
        return this;
    }

    public Long getMessageId() {
        return messageId;
    }

    public MessageContext setMessageId(Long messageId) {
        this.messageId = messageId;
        return this;
    }
}
