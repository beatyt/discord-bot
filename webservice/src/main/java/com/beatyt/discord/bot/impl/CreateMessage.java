package com.beatyt.discord.bot.impl;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateMessage {
    @JsonProperty
    public final String content;
    @JsonProperty
    public final boolean tts;

    public CreateMessage(String content, boolean tts) {
        this.content = content;
        this.tts = tts;
    }
}
