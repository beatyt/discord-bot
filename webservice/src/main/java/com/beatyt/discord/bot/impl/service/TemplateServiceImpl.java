package com.beatyt.discord.bot.impl.service;

import com.beatyt.lang.RunTimeTemplate;
import com.beatyt.lang.SavedTemplate;
import org.springframework.stereotype.Service;

@Service
public class TemplateServiceImpl {

    public String replaceOnce(String s) {
        return new SavedTemplate().apply(s);
    }

    public String replace(String s) {
        return new RunTimeTemplate().apply(s);
    }
}
