/*
 * Copyright (c) 2018.
 */

package com.beatyt.discord.bot.api;

import com.beatyt.discord.bot.impl.message.MessageContext;

public interface HelpMixin {
    void help(MessageContext context);
}
