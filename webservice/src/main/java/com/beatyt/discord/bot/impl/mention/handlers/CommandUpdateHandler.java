/*
 * Copyright (c) 2019.
 */

package com.beatyt.discord.bot.impl.mention.handlers;

import com.beatyt.discord.bot.api.Bot;
import com.beatyt.discord.bot.api.CommandMentionHandler;
import com.beatyt.discord.bot.impl.DiscordWebSockerHandler;
import com.beatyt.discord.bot.impl.dao.Script;
import com.beatyt.discord.bot.impl.message.MessageContext;
import com.beatyt.lang.CommandAdditionImpl;
import com.beatyt.lang.api.commands.Update;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class CommandUpdateHandler implements CommandMentionHandler<String> {
    private Logger logger = LoggerFactory.getLogger(DiscordWebSockerHandler.class);
    private final Bot bot;

    @Autowired
    public CommandUpdateHandler(Bot bot) {
        this.bot = bot;
    }

    @Override
    public void handeMention(String command, MessageContext context) {
        // updates
        Update add = new CommandAdditionImpl().execute(command);
        String trigger = add.getTrigger();
        if (StringUtils.isEmpty(add.getErrors())) {
            // Needs the script name or trigger to update
            Script script = bot.getBrain().findScript(context, trigger);
            if (script != null) {
                bot.getBrain().addResponse(context, script, add.getAction());
                bot.send(context, "Updated " + trigger);
                bot.deleteUserMessage(context);
            } else {
                bot.send(context, "No matching script.");
            }
//            if (script == null) {
//                return "No script found";
//            } else if (!userIsScriptOwner(context, script)) {
//                return "Check your privilege. This isn't your script.";
//            } else {
//                bot.addResponse(context, script, add.getAction());
//                return "Updated " + script.getScriptName();
//            }
        }
    }
}
