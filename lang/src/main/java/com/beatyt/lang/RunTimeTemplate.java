package com.beatyt.lang;

import com.beatyt.lang.templates.ReplaceRandomNumber;
import com.beatyt.lang.templates.ReplaceRandomString;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class RunTimeTemplate implements Function<String, String> {

    private List<Function<String, String>> templates = new ArrayList<>();

    public RunTimeTemplate() {
        this.templates.add( new ReplaceRandomString(ReplaceRandomString.runPattern));
        this.templates.add( new ReplaceRandomNumber(ReplaceRandomNumber.runPattern));
    }

    @Override
    public String apply(String string) {
        return templates.stream()
                .reduce((t1, t2) -> t2.compose(t1))
                .get()
                .apply(string)
                .replace("{{", "")
                .replace("}}", "");
    }
}
