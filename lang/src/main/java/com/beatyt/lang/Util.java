package com.beatyt.lang;

import java.util.List;
import java.util.Random;

public class Util {
    public static <T> T random(List<T> list) {
        if (list.isEmpty()) {
            throw new IllegalArgumentException("Should not call with empty list");
        }
        return list.get(new Random().nextInt(list.size()));
    }
}
