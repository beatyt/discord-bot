package com.beatyt.lang;

import com.beatyt.lang.templates.ReplaceRandomNumber;
import com.beatyt.lang.templates.ReplaceRandomString;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class SavedTemplate implements Function<String, String> {
    private List<Function<String, String>> templates = new ArrayList<>();

    public SavedTemplate() {
        this.templates.add(new ReplaceRandomString(ReplaceRandomString.runOncePattern));
        this.templates.add(new ReplaceRandomNumber(ReplaceRandomNumber.runOncePattern));
    }

    @Override
    public String apply(String string) {
        return templates.stream()
                .reduce((t1, t2) -> t2.compose(t1))
                .get()
                .apply(string)
                ;
    }

}
