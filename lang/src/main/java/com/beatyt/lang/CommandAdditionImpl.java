/*
 * Copyright (c) 2018.
 */

package com.beatyt.lang;

import com.beatyt.lang.api.commands.Update;
import org.antlr.v4.runtime.BufferedTokenStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.apache.commons.lang3.tuple.ImmutablePair;

public class CommandAdditionImpl extends CommandExecutor<Update> {

    /**
     * Takes a command and calls the ANTLR project to see if it matches
     *
     * @param command Command without the bot prefix / bot trigger
     * @return Left is the pair of trigger and action. Right is the errors
     */
    public Update execute(String command) {
        CharStream charStream = CharStreams.fromString(command);
        HelloLexer lexer = new HelloLexer(charStream);
        BufferedTokenStream bufferedTokenStream = new BufferedTokenStream(lexer);
        HelloParser helloParser = new HelloParser(bufferedTokenStream);
        helloParser.removeErrorListeners();
        ErrorListener listener = new ErrorListener();
        helloParser.addErrorListener(listener);

        VisitorImpl visitor = new VisitorImpl();
        HelloParser.AddContext add = helloParser.add();

        ImmutablePair<String, String> stringStringImmutablePair = visitor.visitAdd(add);

        String errors = null;
        if (helloParser.getNumberOfSyntaxErrors() > 0) {
            errors = listener.getErrors();
        }

        Update parseResult = new Update();

        parseResult.setTrigger(stringStringImmutablePair.getLeft());
        parseResult.setAction(stringStringImmutablePair.getRight());
        parseResult.setErrors(errors);

        return parseResult;
    }
}
