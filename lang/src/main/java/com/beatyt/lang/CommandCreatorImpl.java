package com.beatyt.lang;

import com.beatyt.lang.api.commands.Create;
import org.antlr.v4.runtime.BufferedTokenStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.apache.commons.lang3.tuple.ImmutablePair;

public class CommandCreatorImpl extends CommandExecutor<Create> {

    /**
     * Takes a command and calls the ANTLR project to see if it matches
     *
     * @param command Command without the bot prefix / bot trigger
     * @return Left is the pair of trigger and action. Right is the errors
     */
    public Create execute(String command) {
        // TODO: If it is an existing command, then what? Who responsible?
        CharStream charStream = CharStreams.fromString(command);
        HelloLexer lexer = new HelloLexer(charStream);
        BufferedTokenStream bufferedTokenStream = new BufferedTokenStream(lexer);
        HelloParser helloParser = new HelloParser(bufferedTokenStream);
        helloParser.removeErrorListeners();
        ErrorListener listener = new ErrorListener();
        helloParser.addErrorListener(listener);

        VisitorImpl visitor = new VisitorImpl();
        // TODO visit other mutations
        HelloParser.ChatContext chat = helloParser.chat();

        ImmutablePair<String, String> stringStringImmutablePair = visitor.visitChat(chat);

        String errors = null;
        if (helloParser.getNumberOfSyntaxErrors() > 0) {
            errors = listener.getErrors();
        }

        Create parseResult = new Create();

        parseResult.setTrigger(stringStringImmutablePair.getLeft());
        parseResult.setAction(stringStringImmutablePair.getRight());
        parseResult.setErrors(errors);

        return parseResult;
    }
}
