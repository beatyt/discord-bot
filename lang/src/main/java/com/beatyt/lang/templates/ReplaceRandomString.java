package com.beatyt.lang.templates;

import com.beatyt.lang.Util;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReplaceRandomString implements Function<String, String> {

    public static final String runOncePattern = "\\{\\{randomOnce:([A-Za-z]|,|\\s)+\\}\\}";
    public static final String runPattern = "\\{\\{random:([A-Za-z]|,|\\s)+\\}\\}";
    private final Pattern pattern;

    public ReplaceRandomString(String pattern) {
        this.pattern = Pattern.compile(pattern);
    }

    @Override
    public String apply(String string) {
        Matcher matcher = pattern.matcher(string);
        if (!matcher.find()) {
            return string;
        }
        String group = matcher.group();
        String template = StringUtils.substringBetween(group,"{{", "}}");
        String values = StringUtils.substringAfter(template, ":");
        String[] strings = values.split(",(\\s)?");
        List<String> stringList = Arrays.asList(strings);

        string = string.replace(group, Util.random(stringList));

        return string;
    }
}
