package com.beatyt.lang.templates;

import org.apache.commons.lang3.StringUtils;

import java.util.Random;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReplaceRandomNumber implements Function<String, String> {

    public static final String runOncePattern = "\\{\\{randomOnce:\\d,\\d\\}\\}";
    public static final String runPattern = "\\{\\{random:\\d,\\d\\}\\}";
    private final Pattern pattern;

    public ReplaceRandomNumber(String pattern) {
        this.pattern = Pattern.compile(pattern);
    }

    @Override
    public String apply(String string) {
        Matcher matcher = pattern.matcher(string);
        if (!matcher.find()) {
            return string;
        }
        Random random = new Random();
        String group = matcher.group();
        String template = StringUtils.substringBetween(group, "{{", "}}");
        String values = StringUtils.substringAfter(template, ":");
        String[] split = values.split(",(\\s)?");
        int lowerBound = Integer.parseInt(split[0]);
        int upperBound = Integer.parseInt(split[1]);

        int randomRank = random.nextInt((upperBound - lowerBound) + 1) + lowerBound;
        string = string.replace(group, String.valueOf(randomRank));

        return string;
    }
}
