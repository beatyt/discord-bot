/*
 * Copyright (c) 2019.
 */

package com.beatyt.lang;

import com.beatyt.lang.api.commands.Deletion;
import org.antlr.v4.runtime.BufferedTokenStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.apache.commons.lang3.tuple.ImmutablePair;

public class CommandDeletionImpl extends CommandExecutor<Deletion> {
    /**
     * Takes a command and calls the ANTLR project to see if it matches
     *
     * @param command Command without the bot prefix / bot trigger
     * @return Left is the pair of trigger and action. Right is the errors
     */
    public Deletion execute(String command) {
        CharStream charStream = CharStreams.fromString(command);
        HelloLexer lexer = new HelloLexer(charStream);
        BufferedTokenStream bufferedTokenStream = new BufferedTokenStream(lexer);
        HelloParser helloParser = new HelloParser(bufferedTokenStream);
        helloParser.removeErrorListeners();
        ErrorListener listener = new ErrorListener();
        helloParser.addErrorListener(listener);

        VisitorImpl visitor = new VisitorImpl();
        HelloParser.DeleteContext delete = helloParser.delete();

        ImmutablePair<String, String> stringStringImmutablePair = visitor.visitDelete(delete);

        String errors = null;
        if (helloParser.getNumberOfSyntaxErrors() > 0) {
            errors = listener.getErrors();
        }

        Deletion parseResult = new Deletion();
        parseResult.setErrors(errors);

        parseResult.setTrigger(stringStringImmutablePair.getLeft());

        return parseResult;
    }
}
