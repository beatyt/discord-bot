/*
 * Copyright (c) 2018.
 */

package com.beatyt.lang.api.commands;

public interface Command<S, R> {
    S execute(R command);
}
