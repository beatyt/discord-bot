/*
 * Copyright (c) 2018.
 */

package com.beatyt.lang.api.commands;

import com.beatyt.lang.MessageParseResult;

public class Deletion extends MessageParseResult {
    String trigger;

    public String getTrigger() {
        return trigger;
    }

    public void setTrigger(String trigger) {
        this.trigger = trigger;
    }
}
