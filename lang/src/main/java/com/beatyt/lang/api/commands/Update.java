/*
 * Copyright (c) 2018.
 */

package com.beatyt.lang.api.commands;

import com.beatyt.lang.MessageParseResult;

public class Update extends MessageParseResult {
    String trigger;
    String action;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getTrigger() {
        return trigger;
    }

    public void setTrigger(String trigger) {
        this.trigger = trigger;
    }
}
