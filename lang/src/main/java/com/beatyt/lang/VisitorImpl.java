package com.beatyt.lang;

import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.Random;

/**
 * Purpose: Save a trigger and action for future use
 */
public class VisitorImpl implements HelloVisitor<ImmutablePair<String, String>> {


    /**
     *
     * @param ctx the parse tree
     * @return Key will be trigger and value the action
     */
    @Override
    public ImmutablePair<String, String> visitChat(HelloParser.ChatContext ctx) {
        HelloParser.LineContext child = ctx.getChild(HelloParser.LineContext.class, 0);
        HelloParser.ActionContext action = child.getChild(HelloParser.ActionContext.class, 0);
        HelloParser.TriggerContext trigger = child.getChild(HelloParser.TriggerContext.class, 0);
        if (trigger == null || action == null) {
            return ImmutablePair.of(null, null);
        }
        return ImmutablePair.of(trigger.getText(), action.getText());
    }

    @Override
    public ImmutablePair<String, String> visitLine(HelloParser.LineContext ctx) {
        return null;
    }

    @Override
    public ImmutablePair<String, String> visitPrefix(HelloParser.PrefixContext ctx) {
        return null;
    }

    @Override
    public ImmutablePair<String, String> visitCondition(HelloParser.ConditionContext ctx) {
        return null;
    }

    @Override
    public ImmutablePair<String, String> visitTrigger(HelloParser.TriggerContext ctx) {
        return null;
    }

    @Override
    public ImmutablePair<String, String> visitEvent(HelloParser.EventContext ctx) {
        return null;
    }

    @Override
    public ImmutablePair<String, String> visitAction(HelloParser.ActionContext ctx) {
        return null;
    }

    /**
     *
     * @param ctx the parse tree
     * @return Key will be the script name and value will be null
     */
    @Override
    public ImmutablePair<String, String> visitDelete(HelloParser.DeleteContext ctx) {
        HelloParser.ScriptNameContext scriptNameContext = ctx.getChild(HelloParser.ScriptNameContext.class, 0);
        HelloParser.TriggerContext triggerContext = scriptNameContext.getChild(HelloParser.TriggerContext.class, 0);
        return ImmutablePair.of(triggerContext.getText(), null);
    }

    @Override
    public ImmutablePair<String, String> visitScriptName(HelloParser.ScriptNameContext ctx) {
        return null;
    }

    @Override
    public ImmutablePair<String, String> visitDel(HelloParser.DelContext ctx) {
        return null;
    }

    @Override
    public ImmutablePair<String, String> visitAdd(HelloParser.AddContext ctx) {
        HelloParser.ActionContext action = ctx.getChild(HelloParser.ActionContext.class, 0);
        HelloParser.TriggerContext trigger = ctx.getChild(HelloParser.TriggerContext.class, 0);
        if (trigger == null || action == null) {
            return ImmutablePair.of(null, null);
        }
        return ImmutablePair.of(trigger.getText(), action.getText());
    }

    @Override
    public ImmutablePair<String, String> visitAlso(HelloParser.AlsoContext ctx) {
        return null;
    }

    @Override
    public ImmutablePair<String, String> visit(ParseTree tree) {
        return null;
    }

    @Override
    public ImmutablePair<String, String> visitChildren(RuleNode node) {
        return null;
    }

    @Override
    public ImmutablePair<String, String> visitTerminal(TerminalNode node) {
        return null;
    }

    @Override
    public ImmutablePair<String, String> visitErrorNode(ErrorNode node) {
        return null;
    }
}
