/*
 * Copyright (c) 2018.
 */

package com.beatyt.lang;

import com.beatyt.lang.api.commands.Command;
import com.beatyt.lang.api.commands.Deletion;
import org.antlr.v4.runtime.BufferedTokenStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.apache.commons.lang3.tuple.ImmutablePair;

public abstract class CommandExecutor<T extends MessageParseResult> implements Command<MessageParseResult, String> {
    public abstract T execute(String command);
}
