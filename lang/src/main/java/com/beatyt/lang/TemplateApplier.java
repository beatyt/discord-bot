/*
 * Copyright (c) 2018.
 */

package com.beatyt.lang;

import java.util.List;
import java.util.function.Function;

public class TemplateApplier {
    /**
     * Performs modifications on a command string by calling the functions provided
     *
     * @param templates List of functions that will apply modifications
     * @param string A string to operate on
     * @return A parsed out command string
     */
    public String apply(List<Function<String, String>> templates, String string) {
        String apply = templates.stream()
                .reduce((t1, t2) -> t2.compose(t1))
                .get()
                .apply(string)
                .replace("{{", "")
                .replace("}}", "");
        return apply;
    }
}
