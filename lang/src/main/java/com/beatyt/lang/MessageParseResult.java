/*
 * Copyright (c) 2018.
 */

package com.beatyt.lang;

public abstract class MessageParseResult {

    String errors;

    public void setErrors(String errors) {
        this.errors = errors;
    }

    public String getErrors() {
        return errors;
    }
}
