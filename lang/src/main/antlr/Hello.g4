grammar Hello;

@header{
     package com.beatyt.lang;
 }

/*
* Lexer rules
*/
fragment B : ('B'|'b') ;
fragment W : ('W'|'w') ;
fragment O : ('O'|'o') ;
fragment R : ('R'|'r') ;
fragment D : ('D'|'d') ;
fragment S : ('S'|'s') ;
fragment A : ('A'|'a') ;
fragment Y : ('Y'|'y') ;
fragment H : ('H'|'h') ;
fragment E : ('E'|'e') ;
fragment N : ('N'|'n') ;
fragment T : ('T'|'t') ;
fragment M : ('M'|'m') ;
fragment L : ('L'|'l') ;

fragment LOWERCASE  : [a-z] ;
fragment UPPERCASE  : [A-Z] ;

BOT_ID : '<' '@' [0-9]+ '>' ;

UP_UP : '^^';

TriggerOrAction : UP_UP ( ( WORD | EMOJI | UPLOADED_EMOJI | JAPANESE | WHITESPACE | SYMBOL | NUMBERS | OPEN_TEMPLATE | CLOSE_TEMPLATE | NEWLINE ) + | ) UP_UP ;

WHEN : W H E N ;

THEN : T H E N ;

DO : D O ;

DELETE : D E L E T E ;

ALSO : A L S O ;

EMOJI : [\p{Emoji}] ;

UPLOADED_EMOJI : '<' ':' [A-Za-z]+ ':' + [0-9]+ '>'  ;

JAPANESE : [\p{Script=Hiragana}\p{Script=Katakana}\p{Script=Han}] ;

WORD : ( LOWERCASE | UPPERCASE )+ ;

SYMBOL : (  '_' | '\'' | '{' | '}' | ':' | ',' | '!' | '.' | '(' | ')' | '-' | '=' | '+' | '$' | '%' | '@' | '#' | '*' |
 '&' | ']' | '[' | '`' | '>' | '<' | '/' | '\\' | '"' | '?' ) ;

NUMBERS : [0-9] ;

NEWLINE : ('\r'? '\n' | '\r')+ ;

WHITESPACE : (' ' | '\t') ;

OPEN_TEMPLATE : '{{' ;

CLOSE_TEMPLATE : '}}';

/*
* Parser rules
*/
chat : line EOF;

line : ( WHITESPACE+ )? condition trigger WHITESPACE+ event action ;

prefix : BOT_ID WHITESPACE+ ;

condition : WHEN WHITESPACE+ ;

trigger : TriggerOrAction ;

event : THEN WHITESPACE+ ;

action : TriggerOrAction ;


// delete

delete : del scriptName;

scriptName : trigger ;

del : DELETE WHITESPACE+ ;

// insert

add : condition trigger WHITESPACE+ also WHITESPACE+ action ;

also : ALSO ;
