package com.beatyt.lang;

import com.beatyt.lang.api.commands.Create;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.util.Map;
import java.util.function.Supplier;

import static org.junit.Assert.*;

public class CommandCreatorImplTest {

    private CommandCreatorImpl cut = new CommandCreatorImpl();

    @Test
    public void create() {
        Create execute = cut.execute("when ^^smile^^ then ^^\uD83D\uDE00^^");
        String trigger = execute.getTrigger();
        String action = execute.getAction();
        String errors = execute.getErrors();

        assertNull(errors);

        assertEquals("^^smile^^", trigger);
        assertEquals("^^\uD83D\uDE00^^", action);
    }

    @Test
    public void emptyInput() {
        Create execute = cut.execute("");
        String errors = execute.getErrors();

        assertNotNull(errors);

        assertNull(execute.getTrigger());
    }
}