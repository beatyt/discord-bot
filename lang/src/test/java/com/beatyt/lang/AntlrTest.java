package com.beatyt.lang;

import org.antlr.v4.runtime.BufferedTokenStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.util.Map;


public class AntlrTest {

    @Test
    public void test() {
        CharStream charStream = CharStreams.fromString("when `x` then `y`");
        HelloLexer lexer = new HelloLexer(charStream);
        BufferedTokenStream bufferedTokenStream = new BufferedTokenStream(lexer);
        HelloParser helloParser = new HelloParser(bufferedTokenStream);

        VisitorImpl visitor = new VisitorImpl();
        Pair<String, String> actionTriggerPair = visitor.visitChat(helloParser.chat());

        actionTriggerPair.getLeft();
    }

}
