/*
 * Copyright (c) 2018.
 */

package com.beatyt.lang;

import org.junit.Test;

import static org.junit.Assert.*;

public class RunTimeTemplateTest {

    RunTimeTemplate cut = new RunTimeTemplate();

    @Test
    public void apply() {
        String s = cut.apply("{{random:abc def, abc def}}");
        assertEquals("abc def", s);

        s = cut.apply("{{random:abc def, abc def}} {{random:abc def, abc def}}");
        assertEquals("abc def abc def", s);
    }

    @Test
    public void randomNumbers() {
        String s = cut.apply("{{random:0,0}}");
        assertEquals("0", s);

        s = cut.apply("{{random:0,0}}{{random:0,0}}");
        assertEquals("00", s);
    }

    @Test
    public void randomMultipleTokens() {
        String s = cut.apply("{{random:abc def,abc def}} {{random:0,0}}");
        assertEquals("abc def 0", s);
    }
}