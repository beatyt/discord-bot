/*
 * Copyright (c) 2018.
 */

package com.beatyt.lang.templates;

import org.junit.Test;

import static org.junit.Assert.*;

public class ReplaceRandomNumberTest {

    ReplaceRandomNumber cut = new ReplaceRandomNumber(ReplaceRandomNumber.runPattern);

    @Test
    public void apply() {
        String s = cut.apply("{{random:0,0}}");

        assertEquals("0", s);
    }
}