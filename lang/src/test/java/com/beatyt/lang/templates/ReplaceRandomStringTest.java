/*
 * Copyright (c) 2018.
 */

package com.beatyt.lang.templates;

import org.junit.Test;

import static org.junit.Assert.*;

public class ReplaceRandomStringTest {

    ReplaceRandomString cut = new ReplaceRandomString(ReplaceRandomString.runPattern);

    @Test
    public void apply() {
        String s = cut.apply("{{random:abc,abc}}");
        assertEquals("abc", s);

        s = cut.apply("{{random:abc def,abc def}}");
        assertEquals("abc def", s);
    }

    @Test
    public void testNewLines() {
        String s = cut.apply("{{random:abc\n,abc\n}}");
        assertEquals("abc\n", s);
    }
}