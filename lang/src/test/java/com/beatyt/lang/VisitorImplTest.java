/*
 * Copyright (c) 2018.
 */

package com.beatyt.lang;

import org.antlr.v4.runtime.BufferedTokenStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class VisitorImplTest {

    private VisitorImpl cut = new VisitorImpl();

    @Test
    public void visitChat() {
        CharStream charStream = CharStreams.fromString("when `x` then `y`");
        HelloLexer lexer = new HelloLexer(charStream);
        BufferedTokenStream bufferedTokenStream = new BufferedTokenStream(lexer);
        HelloParser helloParser = new HelloParser(bufferedTokenStream);
        helloParser.removeErrorListeners();
        ErrorListener listener = new ErrorListener();
        helloParser.addErrorListener(listener);

        ImmutablePair<String, String> stringStringImmutablePair = cut.visitChat(helloParser.chat());

        assertEquals("`x`", stringStringImmutablePair.getLeft());
        assertEquals("`y`", stringStringImmutablePair.getRight());
    }

    @Test
    public void visitDelete() {
        CharStream charStream = CharStreams.fromString("delete `x`");
        HelloLexer lexer = new HelloLexer(charStream);
        BufferedTokenStream bufferedTokenStream = new BufferedTokenStream(lexer);
        HelloParser helloParser = new HelloParser(bufferedTokenStream);
        helloParser.removeErrorListeners();
        ErrorListener listener = new ErrorListener();
        helloParser.addErrorListener(listener);

        ImmutablePair<String, String> pair = cut.visitDelete(helloParser.delete());

        assertEquals("`x`", pair.getLeft());
    }
}