// Generated from Hello.g4 by ANTLR 4.7.1

     package com.beatyt.lang;
 
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link HelloParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface HelloVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link HelloParser#chat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitChat(HelloParser.ChatContext ctx);
	/**
	 * Visit a parse tree produced by {@link HelloParser#line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLine(HelloParser.LineContext ctx);
	/**
	 * Visit a parse tree produced by {@link HelloParser#prefix}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrefix(HelloParser.PrefixContext ctx);
	/**
	 * Visit a parse tree produced by {@link HelloParser#condition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondition(HelloParser.ConditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link HelloParser#trigger}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTrigger(HelloParser.TriggerContext ctx);
	/**
	 * Visit a parse tree produced by {@link HelloParser#event}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEvent(HelloParser.EventContext ctx);
	/**
	 * Visit a parse tree produced by {@link HelloParser#action}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAction(HelloParser.ActionContext ctx);
	/**
	 * Visit a parse tree produced by {@link HelloParser#delete}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDelete(HelloParser.DeleteContext ctx);
	/**
	 * Visit a parse tree produced by {@link HelloParser#scriptName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitScriptName(HelloParser.ScriptNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link HelloParser#del}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDel(HelloParser.DelContext ctx);
	/**
	 * Visit a parse tree produced by {@link HelloParser#add}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdd(HelloParser.AddContext ctx);
	/**
	 * Visit a parse tree produced by {@link HelloParser#also}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlso(HelloParser.AlsoContext ctx);
}