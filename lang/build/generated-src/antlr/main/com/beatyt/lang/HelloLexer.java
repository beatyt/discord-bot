// Generated from Hello.g4 by ANTLR 4.7.1

     package com.beatyt.lang;
 
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class HelloLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		BOT_ID=1, UP_UP=2, TriggerOrAction=3, WHEN=4, THEN=5, DO=6, DELETE=7, 
		ALSO=8, EMOJI=9, UPLOADED_EMOJI=10, JAPANESE=11, WORD=12, SYMBOL=13, NUMBERS=14, 
		NEWLINE=15, WHITESPACE=16, OPEN_TEMPLATE=17, CLOSE_TEMPLATE=18;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"B", "W", "O", "R", "D", "S", "A", "Y", "H", "E", "N", "T", "M", "L", 
		"LOWERCASE", "UPPERCASE", "BOT_ID", "UP_UP", "TriggerOrAction", "WHEN", 
		"THEN", "DO", "DELETE", "ALSO", "EMOJI", "UPLOADED_EMOJI", "JAPANESE", 
		"WORD", "SYMBOL", "NUMBERS", "NEWLINE", "WHITESPACE", "OPEN_TEMPLATE", 
		"CLOSE_TEMPLATE"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, "'^^'", null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, "'{{'", "'}}'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "BOT_ID", "UP_UP", "TriggerOrAction", "WHEN", "THEN", "DO", "DELETE", 
		"ALSO", "EMOJI", "UPLOADED_EMOJI", "JAPANESE", "WORD", "SYMBOL", "NUMBERS", 
		"NEWLINE", "WHITESPACE", "OPEN_TEMPLATE", "CLOSE_TEMPLATE"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public HelloLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Hello.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\24\u00d2\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t"+
		" \4!\t!\4\"\t\"\4#\t#\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7"+
		"\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17"+
		"\3\20\3\20\3\21\3\21\3\22\3\22\3\22\6\22k\n\22\r\22\16\22l\3\22\3\22\3"+
		"\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\6"+
		"\24\177\n\24\r\24\16\24\u0080\3\24\5\24\u0084\n\24\3\24\3\24\3\25\3\25"+
		"\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\30\3\30\3\30"+
		"\3\30\3\30\3\30\3\30\3\31\3\31\3\31\3\31\3\31\3\32\3\32\3\33\3\33\3\33"+
		"\6\33\u00a6\n\33\r\33\16\33\u00a7\3\33\6\33\u00ab\n\33\r\33\16\33\u00ac"+
		"\3\33\6\33\u00b0\n\33\r\33\16\33\u00b1\3\33\3\33\3\34\3\34\3\35\3\35\6"+
		"\35\u00ba\n\35\r\35\16\35\u00bb\3\36\3\36\3\37\3\37\3 \5 \u00c3\n \3 "+
		"\3 \6 \u00c7\n \r \16 \u00c8\3!\3!\3\"\3\"\3\"\3#\3#\3#\2\2$\3\2\5\2\7"+
		"\2\t\2\13\2\r\2\17\2\21\2\23\2\25\2\27\2\31\2\33\2\35\2\37\2!\2#\3%\4"+
		"\'\5)\6+\7-\b/\t\61\n\63\13\65\f\67\r9\16;\17=\20?\21A\22C\23E\24\3\2"+
		"\26\4\2DDdd\4\2YYyy\4\2QQqq\4\2TTtt\4\2FFff\4\2UUuu\4\2CCcc\4\2[[{{\4"+
		"\2JJjj\4\2GGgg\4\2PPpp\4\2VVvv\4\2OOoo\4\2NNnn\3\2c|\3\2C\\\3\2\62;\4"+
		"\2C\\c|\t\2#\61<<>B]_ab}}\177\177\4\2\13\13\"\"\4\u0095\2%\2%\2,\2,\2"+
		"\62\2;\2\u00ab\2\u00ab\2\u00b0\2\u00b0\2\u203e\2\u203e\2\u204b\2\u204b"+
		"\2\u2124\2\u2124\2\u213b\2\u213b\2\u2196\2\u219b\2\u21ab\2\u21ac\2\u231c"+
		"\2\u231d\2\u232a\2\u232a\2\u23d1\2\u23d1\2\u23eb\2\u23f5\2\u23fa\2\u23fc"+
		"\2\u24c4\2\u24c4\2\u25ac\2\u25ad\2\u25b8\2\u25b8\2\u25c2\2\u25c2\2\u25fd"+
		"\2\u2600\2\u2602\2\u2606\2\u2610\2\u2610\2\u2613\2\u2613\2\u2616\2\u2617"+
		"\2\u261a\2\u261a\2\u261f\2\u261f\2\u2622\2\u2622\2\u2624\2\u2625\2\u2628"+
		"\2\u2628\2\u262c\2\u262c\2\u2630\2\u2631\2\u263a\2\u263c\2\u2642\2\u2642"+
		"\2\u2644\2\u2644\2\u264a\2\u2655\2\u2662\2\u2662\2\u2665\2\u2665\2\u2667"+
		"\2\u2668\2\u266a\2\u266a\2\u267d\2\u267d\2\u2681\2\u2681\2\u2694\2\u2699"+
		"\2\u269b\2\u269b\2\u269d\2\u269e\2\u26a2\2\u26a3\2\u26ac\2\u26ad\2\u26b2"+
		"\2\u26b3\2\u26bf\2\u26c0\2\u26c6\2\u26c7\2\u26ca\2\u26ca\2\u26d0\2\u26d1"+
		"\2\u26d3\2\u26d3\2\u26d5\2\u26d6\2\u26eb\2\u26ec\2\u26f2\2\u26f7\2\u26f9"+
		"\2\u26fc\2\u26ff\2\u26ff\2\u2704\2\u2704\2\u2707\2\u2707\2\u270a\2\u270f"+
		"\2\u2711\2\u2711\2\u2714\2\u2714\2\u2716\2\u2716\2\u2718\2\u2718\2\u271f"+
		"\2\u271f\2\u2723\2\u2723\2\u272a\2\u272a\2\u2735\2\u2736\2\u2746\2\u2746"+
		"\2\u2749\2\u2749\2\u274e\2\u274e\2\u2750\2\u2750\2\u2755\2\u2757\2\u2759"+
		"\2\u2759\2\u2765\2\u2766\2\u2797\2\u2799\2\u27a3\2\u27a3\2\u27b2\2\u27b2"+
		"\2\u27c1\2\u27c1\2\u2936\2\u2937\2\u2b07\2\u2b09\2\u2b1d\2\u2b1e\2\u2b52"+
		"\2\u2b52\2\u2b57\2\u2b57\2\u3032\2\u3032\2\u303f\2\u303f\2\u3299\2\u3299"+
		"\2\u329b\2\u329b\2\uf006\3\uf006\3\uf0d1\3\uf0d1\3\uf172\3\uf173\3\uf180"+
		"\3\uf181\3\uf190\3\uf190\3\uf193\3\uf19c\3\uf1e8\3\uf201\3\uf203\3\uf204"+
		"\3\uf21c\3\uf21c\3\uf231\3\uf231\3\uf234\3\uf23c\3\uf252\3\uf253\3\uf302"+
		"\3\uf323\3\uf326\3\uf395\3\uf398\3\uf399\3\uf39b\3\uf39d\3\uf3a0\3\uf3f2"+
		"\3\uf3f5\3\uf3f7\3\uf3f9\3\uf4ff\3\uf501\3\uf53f\3\uf54b\3\uf550\3\uf552"+
		"\3\uf569\3\uf571\3\uf572\3\uf575\3\uf57c\3\uf589\3\uf589\3\uf58c\3\uf58f"+
		"\3\uf592\3\uf592\3\uf597\3\uf598\3\uf5a6\3\uf5a7\3\uf5aa\3\uf5aa\3\uf5b3"+
		"\3\uf5b4\3\uf5be\3\uf5be\3\uf5c4\3\uf5c6\3\uf5d3\3\uf5d5\3\uf5de\3\uf5e0"+
		"\3\uf5e3\3\uf5e3\3\uf5e5\3\uf5e5\3\uf5ea\3\uf5ea\3\uf5f1\3\uf5f1\3\uf5f5"+
		"\3\uf5f5\3\uf5fc\3\uf651\3\uf682\3\uf6c7\3\uf6cd\3\uf6d4\3\uf6e2\3\uf6e7"+
		"\3\uf6eb\3\uf6eb\3\uf6ed\3\uf6ee\3\uf6f2\3\uf6f2\3\uf6f5\3\uf6f8\3\uf912"+
		"\3\uf920\3\uf922\3\uf929\3\uf932\3\uf932\3\uf935\3\uf93c\3\uf93e\3\uf940"+
		"\3\uf942\3\uf947\3\uf949\3\uf94d\3\uf952\3\uf960\3\uf982\3\uf993\3\uf9c2"+
		"\3\uf9c2\3\35\2\u2e82\2\u2e9b\2\u2e9d\2\u2ef5\2\u2f02\2\u2fd7\2\u3007"+
		"\2\u3007\2\u3009\2\u3009\2\u3023\2\u302b\2\u303a\2\u303d\2\u3043\2\u3098"+
		"\2\u309f\2\u30a1\2\u30a3\2\u30fc\2\u30ff\2\u3101\2\u31f2\2\u3201\2\u32d2"+
		"\2\u3300\2\u3302\2\u3359\2\u3402\2\u4db7\2\u4e02\2\u9fd7\2\uf902\2\ufa6f"+
		"\2\ufa72\2\ufadb\2\uff68\2\uff71\2\uff73\2\uff9f\2\ub002\3\ub003\3\uf202"+
		"\3\uf202\3\2\4\ua6d8\4\ua702\4\ub736\4\ub742\4\ub81f\4\ub822\4\ucea3\4"+
		"\uf802\4\ufa1f\4\u00d5\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2"+
		"+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2"+
		"\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2"+
		"C\3\2\2\2\2E\3\2\2\2\3G\3\2\2\2\5I\3\2\2\2\7K\3\2\2\2\tM\3\2\2\2\13O\3"+
		"\2\2\2\rQ\3\2\2\2\17S\3\2\2\2\21U\3\2\2\2\23W\3\2\2\2\25Y\3\2\2\2\27["+
		"\3\2\2\2\31]\3\2\2\2\33_\3\2\2\2\35a\3\2\2\2\37c\3\2\2\2!e\3\2\2\2#g\3"+
		"\2\2\2%p\3\2\2\2\'s\3\2\2\2)\u0087\3\2\2\2+\u008c\3\2\2\2-\u0091\3\2\2"+
		"\2/\u0094\3\2\2\2\61\u009b\3\2\2\2\63\u00a0\3\2\2\2\65\u00a2\3\2\2\2\67"+
		"\u00b5\3\2\2\29\u00b9\3\2\2\2;\u00bd\3\2\2\2=\u00bf\3\2\2\2?\u00c6\3\2"+
		"\2\2A\u00ca\3\2\2\2C\u00cc\3\2\2\2E\u00cf\3\2\2\2GH\t\2\2\2H\4\3\2\2\2"+
		"IJ\t\3\2\2J\6\3\2\2\2KL\t\4\2\2L\b\3\2\2\2MN\t\5\2\2N\n\3\2\2\2OP\t\6"+
		"\2\2P\f\3\2\2\2QR\t\7\2\2R\16\3\2\2\2ST\t\b\2\2T\20\3\2\2\2UV\t\t\2\2"+
		"V\22\3\2\2\2WX\t\n\2\2X\24\3\2\2\2YZ\t\13\2\2Z\26\3\2\2\2[\\\t\f\2\2\\"+
		"\30\3\2\2\2]^\t\r\2\2^\32\3\2\2\2_`\t\16\2\2`\34\3\2\2\2ab\t\17\2\2b\36"+
		"\3\2\2\2cd\t\20\2\2d \3\2\2\2ef\t\21\2\2f\"\3\2\2\2gh\7>\2\2hj\7B\2\2"+
		"ik\t\22\2\2ji\3\2\2\2kl\3\2\2\2lj\3\2\2\2lm\3\2\2\2mn\3\2\2\2no\7@\2\2"+
		"o$\3\2\2\2pq\7`\2\2qr\7`\2\2r&\3\2\2\2s\u0083\5%\23\2t\177\59\35\2u\177"+
		"\5\63\32\2v\177\5\65\33\2w\177\5\67\34\2x\177\5A!\2y\177\5;\36\2z\177"+
		"\5=\37\2{\177\5C\"\2|\177\5E#\2}\177\5? \2~t\3\2\2\2~u\3\2\2\2~v\3\2\2"+
		"\2~w\3\2\2\2~x\3\2\2\2~y\3\2\2\2~z\3\2\2\2~{\3\2\2\2~|\3\2\2\2~}\3\2\2"+
		"\2\177\u0080\3\2\2\2\u0080~\3\2\2\2\u0080\u0081\3\2\2\2\u0081\u0084\3"+
		"\2\2\2\u0082\u0084\3\2\2\2\u0083~\3\2\2\2\u0083\u0082\3\2\2\2\u0084\u0085"+
		"\3\2\2\2\u0085\u0086\5%\23\2\u0086(\3\2\2\2\u0087\u0088\5\5\3\2\u0088"+
		"\u0089\5\23\n\2\u0089\u008a\5\25\13\2\u008a\u008b\5\27\f\2\u008b*\3\2"+
		"\2\2\u008c\u008d\5\31\r\2\u008d\u008e\5\23\n\2\u008e\u008f\5\25\13\2\u008f"+
		"\u0090\5\27\f\2\u0090,\3\2\2\2\u0091\u0092\5\13\6\2\u0092\u0093\5\7\4"+
		"\2\u0093.\3\2\2\2\u0094\u0095\5\13\6\2\u0095\u0096\5\25\13\2\u0096\u0097"+
		"\5\35\17\2\u0097\u0098\5\25\13\2\u0098\u0099\5\31\r\2\u0099\u009a\5\25"+
		"\13\2\u009a\60\3\2\2\2\u009b\u009c\5\17\b\2\u009c\u009d\5\35\17\2\u009d"+
		"\u009e\5\r\7\2\u009e\u009f\5\7\4\2\u009f\62\3\2\2\2\u00a0\u00a1\t\26\2"+
		"\2\u00a1\64\3\2\2\2\u00a2\u00a3\7>\2\2\u00a3\u00a5\7<\2\2\u00a4\u00a6"+
		"\t\23\2\2\u00a5\u00a4\3\2\2\2\u00a6\u00a7\3\2\2\2\u00a7\u00a5\3\2\2\2"+
		"\u00a7\u00a8\3\2\2\2\u00a8\u00aa\3\2\2\2\u00a9\u00ab\7<\2\2\u00aa\u00a9"+
		"\3\2\2\2\u00ab\u00ac\3\2\2\2\u00ac\u00aa\3\2\2\2\u00ac\u00ad\3\2\2\2\u00ad"+
		"\u00af\3\2\2\2\u00ae\u00b0\t\22\2\2\u00af\u00ae\3\2\2\2\u00b0\u00b1\3"+
		"\2\2\2\u00b1\u00af\3\2\2\2\u00b1\u00b2\3\2\2\2\u00b2\u00b3\3\2\2\2\u00b3"+
		"\u00b4\7@\2\2\u00b4\66\3\2\2\2\u00b5\u00b6\t\27\2\2\u00b68\3\2\2\2\u00b7"+
		"\u00ba\5\37\20\2\u00b8\u00ba\5!\21\2\u00b9\u00b7\3\2\2\2\u00b9\u00b8\3"+
		"\2\2\2\u00ba\u00bb\3\2\2\2\u00bb\u00b9\3\2\2\2\u00bb\u00bc\3\2\2\2\u00bc"+
		":\3\2\2\2\u00bd\u00be\t\24\2\2\u00be<\3\2\2\2\u00bf\u00c0\t\22\2\2\u00c0"+
		">\3\2\2\2\u00c1\u00c3\7\17\2\2\u00c2\u00c1\3\2\2\2\u00c2\u00c3\3\2\2\2"+
		"\u00c3\u00c4\3\2\2\2\u00c4\u00c7\7\f\2\2\u00c5\u00c7\7\17\2\2\u00c6\u00c2"+
		"\3\2\2\2\u00c6\u00c5\3\2\2\2\u00c7\u00c8\3\2\2\2\u00c8\u00c6\3\2\2\2\u00c8"+
		"\u00c9\3\2\2\2\u00c9@\3\2\2\2\u00ca\u00cb\t\25\2\2\u00cbB\3\2\2\2\u00cc"+
		"\u00cd\7}\2\2\u00cd\u00ce\7}\2\2\u00ceD\3\2\2\2\u00cf\u00d0\7\177\2\2"+
		"\u00d0\u00d1\7\177\2\2\u00d1F\3\2\2\2\17\2l~\u0080\u0083\u00a7\u00ac\u00b1"+
		"\u00b9\u00bb\u00c2\u00c6\u00c8\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}