// Generated from Hello.g4 by ANTLR 4.7.1

     package com.beatyt.lang;
 
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link HelloParser}.
 */
public interface HelloListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link HelloParser#chat}.
	 * @param ctx the parse tree
	 */
	void enterChat(HelloParser.ChatContext ctx);
	/**
	 * Exit a parse tree produced by {@link HelloParser#chat}.
	 * @param ctx the parse tree
	 */
	void exitChat(HelloParser.ChatContext ctx);
	/**
	 * Enter a parse tree produced by {@link HelloParser#line}.
	 * @param ctx the parse tree
	 */
	void enterLine(HelloParser.LineContext ctx);
	/**
	 * Exit a parse tree produced by {@link HelloParser#line}.
	 * @param ctx the parse tree
	 */
	void exitLine(HelloParser.LineContext ctx);
	/**
	 * Enter a parse tree produced by {@link HelloParser#prefix}.
	 * @param ctx the parse tree
	 */
	void enterPrefix(HelloParser.PrefixContext ctx);
	/**
	 * Exit a parse tree produced by {@link HelloParser#prefix}.
	 * @param ctx the parse tree
	 */
	void exitPrefix(HelloParser.PrefixContext ctx);
	/**
	 * Enter a parse tree produced by {@link HelloParser#condition}.
	 * @param ctx the parse tree
	 */
	void enterCondition(HelloParser.ConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link HelloParser#condition}.
	 * @param ctx the parse tree
	 */
	void exitCondition(HelloParser.ConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link HelloParser#trigger}.
	 * @param ctx the parse tree
	 */
	void enterTrigger(HelloParser.TriggerContext ctx);
	/**
	 * Exit a parse tree produced by {@link HelloParser#trigger}.
	 * @param ctx the parse tree
	 */
	void exitTrigger(HelloParser.TriggerContext ctx);
	/**
	 * Enter a parse tree produced by {@link HelloParser#event}.
	 * @param ctx the parse tree
	 */
	void enterEvent(HelloParser.EventContext ctx);
	/**
	 * Exit a parse tree produced by {@link HelloParser#event}.
	 * @param ctx the parse tree
	 */
	void exitEvent(HelloParser.EventContext ctx);
	/**
	 * Enter a parse tree produced by {@link HelloParser#action}.
	 * @param ctx the parse tree
	 */
	void enterAction(HelloParser.ActionContext ctx);
	/**
	 * Exit a parse tree produced by {@link HelloParser#action}.
	 * @param ctx the parse tree
	 */
	void exitAction(HelloParser.ActionContext ctx);
	/**
	 * Enter a parse tree produced by {@link HelloParser#delete}.
	 * @param ctx the parse tree
	 */
	void enterDelete(HelloParser.DeleteContext ctx);
	/**
	 * Exit a parse tree produced by {@link HelloParser#delete}.
	 * @param ctx the parse tree
	 */
	void exitDelete(HelloParser.DeleteContext ctx);
	/**
	 * Enter a parse tree produced by {@link HelloParser#scriptName}.
	 * @param ctx the parse tree
	 */
	void enterScriptName(HelloParser.ScriptNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link HelloParser#scriptName}.
	 * @param ctx the parse tree
	 */
	void exitScriptName(HelloParser.ScriptNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link HelloParser#del}.
	 * @param ctx the parse tree
	 */
	void enterDel(HelloParser.DelContext ctx);
	/**
	 * Exit a parse tree produced by {@link HelloParser#del}.
	 * @param ctx the parse tree
	 */
	void exitDel(HelloParser.DelContext ctx);
	/**
	 * Enter a parse tree produced by {@link HelloParser#add}.
	 * @param ctx the parse tree
	 */
	void enterAdd(HelloParser.AddContext ctx);
	/**
	 * Exit a parse tree produced by {@link HelloParser#add}.
	 * @param ctx the parse tree
	 */
	void exitAdd(HelloParser.AddContext ctx);
	/**
	 * Enter a parse tree produced by {@link HelloParser#also}.
	 * @param ctx the parse tree
	 */
	void enterAlso(HelloParser.AlsoContext ctx);
	/**
	 * Exit a parse tree produced by {@link HelloParser#also}.
	 * @param ctx the parse tree
	 */
	void exitAlso(HelloParser.AlsoContext ctx);
}